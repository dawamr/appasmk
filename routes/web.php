<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing.landing1');
});
Route::get('/logout', 'HomeController@logout');
Route::post('/new-user', 'SiswaController@register');
Route::post('/update-user', 'SiswaController@update_user');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'owner', 'middleware'=>['auth','role:su']], function () {
    Route::get('/','OwnerController@dashboard');

});
Route::group(['prefix'=>'admin', 'middleware'=>['auth','role:admin']], function () {
    Route::get('/','AdminController@dashboard');
    Route::get('/dudi','AdminController@dudi');
    Route::get('/magang','AdminController@magang');
    Route::get('/alumni','AdminController@alumni');
    Route::get('/siswa','AdminController@siswa');

    Route::get('/ajax/user', function(){
        $user = \App\User::pluck('name','id');
         return view('admin.json.user')->with('countries', $user);
    });

    Route::post('/api/addJurusan', 'AdminController@addJurusan');
    Route::post('/api/{id}/delJurusan', 'AdminController@delJurusan');

    Route::post('/api/addKelas', 'AdminController@addKelas');
    Route::post('/api/{id}/delKelas', 'AdminController@delKelas');

    Route::post('/api/addSiswa', 'AdminController@addSiswa');

    Route::post('/api/addDudi', 'AdminController@addDudi');
    Route::post('/api/{id}/delDudi', 'AdminController@delDudi');

    Route::post('/api/addMagang', 'AdminController@addMagang');
    Route::post('/api/addAlumni', 'AdminController@addAlumni');

    Route::post('/api/addUniversitas','AdminController@addUniversitas');

    Route::post('/api/generateToken', 'AdminController@genToken');
    Route::get('/api/searchSiswa', 'AdminController@queryUser');
    Route::get('/api/searchDudi', 'AdminController@queryDudi');

    Route::get('/api/lap1/html/{id}','AdminController@lap1Html');
    Route::get('/api/lap1/xls/{id}','AdminController@lap1Xls');
});
Route::group(['prefix'=>'students', 'middleware'=>['auth','role:siswa']], function () {
    Route::get('/','SiswaController@dashboard');
    Route::get('/list-dudi','SiswaController@dudi');
    Route::get('/list-universitas','SiswaController@univ');
    Route::post('/update-profile','SiswaController@update');
    Route::post('/search','SiswaController@search');
    Route::get('/profile/{id}','SiswaController@profileMore');
    Route::post('/profile/addArtikel','SiswaController@addArtikel');
    Route::get('/profile/likeArtikel/{id}','SiswaController@likeArtikel');
    Route::get('/profile/dislikeArtikel/{id}','SiswaController@dislikeArtikel');
    Route::get('/profile/codeArtikel/{id}','SiswaController@codeArtikel');
    Route::get('/profile/follow/{id}','SiswaController@follow');
    Route::get('/resume','SiswaController@resume');
    Route::get('/resume/create','SiswaController@createCV');
    Route::post('/resume/summary','SiswaController@summary');
    Route::post('/resume/personal-info','SiswaController@personalInfo');
    Route::post('/resume/experience','SiswaController@experience');
    Route::post('/resume/experience/{id}','SiswaController@delExperience');
    Route::post('/resume/education','SiswaController@education');
    Route::post('/resume/education/{id}','SiswaController@delEducation');
    Route::post('/resume/sertifikat','SiswaController@sertifikat');
    Route::post('/resume/sertifikat/{id}','SiswaController@delSertifikat');
    Route::post('/resume/skill','SiswaController@skill');
    Route::post('/resume/skill/{id}','SiswaController@delSkill');
});

Route::get('/shell/php-artisan-migrate-fresh', function() {
     Artisan::call('migrate:fresh');
    return redirect()->back();
});
Route::get('/shell/php-artisan-db-seed', function() {
     Artisan::call('db:seed');
    return redirect()->back();
});

Route::group(['middleware'=>['auth']], function () {
    Route::get('upgrade-account','UpgradeController@account');
    Route::get('upgrade-account/next','UpgradeController@next');
    Route::post('upgrade-account/konfirmasi','UpgradeController@konfirmasi');
    Route::post('upgrade-account','UpgradeController@thanks');
    Route::get('upgrade-account/success','UpgradeController@success');
});


// ROUTE UNTUK WEB  E COMMERCE

Route::group(['prefix'=>'products'], function () {
    Route::get('/','MemberCommerceController@index');
    
});
