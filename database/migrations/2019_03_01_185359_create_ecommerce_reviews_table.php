<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcommerceReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecommerce_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('r_itm_id')->unsigned();
            $table->foreign('r_itm_id')->references('id')->on('ecommerce_items');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('ecommerce_items');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecommerce_reviews');
    }
}
