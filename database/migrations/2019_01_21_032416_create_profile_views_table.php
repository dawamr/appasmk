<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_views', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('viewes_id')->unsigned()->nullable();
            $table->foreign('viewes_id')->references('id')->on('users');
            $table->integer('viewer_id')->unsigned()->nullable();
            $table->foreign('viewer_id')->references('id')->on('users');
            $table->integer('following_id')->unsigned()->nullable();
            $table->foreign('following_id')->references('id')->on('users');
            $table->integer('follower_id')->unsigned()->nullable();
            $table->foreign('follower_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_views');
    }
}
