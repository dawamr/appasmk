<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDudisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dudis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nm_dudi');
            $table->string('alamat');
            $table->string('telp');
            $table->string('email')->nullable();
            $table->string('web')->nullable();
            $table->string('kpl_dudi');
            $table->integer('sekolah_id')->unsigned();
            $table->foreign('sekolah_id')->references('id')->on('sekolahs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dudis');
    }
}
