<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcommerceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecommerce_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('itm_nama');
            $table->integer('itm_harga');
            $table->text('itm_detail');
            $table->string('itm_gambar');
            $table->integer('itm_qty');
            $table->integer('itm_cat_id')->unsigned();
            $table->foreign('itm_cat_id')->references('id')->on('ecommerce_categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecommerce_items');
    }
}
