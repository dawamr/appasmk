<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcommerceDetOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecommerce_det_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dord_ord_id')->unsigned();
            $table->foreign('dord_ord_id')->references('id')->on('ecommerce_orders');
            $table->integer('dord_dis_id')->unsigned();
            $table->foreign('dord_dis_id')->references('id')->on('ecommerce_diskons');
            $table->integer('dord_harga');
            $table->integer('dord_qty');
            $table->integer('dord_total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecommerce_det_orders');
    }
}
