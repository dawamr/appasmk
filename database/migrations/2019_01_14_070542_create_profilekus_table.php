<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilekusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profilekus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('kelas_id')->unsigned()->nullable();
            $table->foreign('kelas_id')->references('id')->on('kelas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->integer('universitas_id')->unsigned()->nullable();
            $table->foreign('universitas_id')->references('id')->on('universitas')->onUpdate('SET NULL')->onDelete('SET NULL');
            $table->enum('status',['siswa','alumni','blocked','report']);
            $table->string('alamat')->nullable();
            $table->string('tmp_lahir')->nullable();
            $table->date('tgl_lahir')->nullable();
            $table->bigInteger('no_ktp')->nullable();
            $table->string('nm_ibu')->nullable();
            $table->string('nm_ayah')->nullable();
            $table->string('nm_wali')->nullable();
            $table->string('telp')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twiter')->nullable();
            $table->string('linkedlnr')->nullable();
            $table->string('instagram')->nullable();
            $table->string('pekerjaan')->nullable();
            $table->string('almt_pekerjaan')->nullable();
            $table->string('pghsl_bulanan')->nullable();
            $table->string('photo')->nullable();
            $table->string('kutipan')->nullable();
            $table->text('summary')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profilekus');
    }
}
