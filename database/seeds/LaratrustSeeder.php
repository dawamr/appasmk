<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Role;
use App\User;

class LaratrustSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
      // Membuat role super user
      $ownerRole = new Role();
      $ownerRole->name = "su";
      $ownerRole->display_name = "Dawam Raja";
      $ownerRole->description = "";
      $ownerRole->save();

      // Membuat role admin
      $adminRole = new Role();
      $adminRole->name = "admin";
      $adminRole->display_name = "Admin";
      $adminRole->description = "";
      $adminRole->save();

      // Membuat role siswa
      $siswaRole = new Role();
      $siswaRole->name = "siswa";
      $siswaRole->display_name = "Siswa";
      $siswaRole->description = "";
      $siswaRole->save();

      $owner = new User();
      $owner->name ="Dawam";
      $owner->email = 'dawam@uangkelas.com';
      $owner->password = bcrypt('rahasia');
      $owner->save();
      $owner->attachRole($ownerRole);
      
      $admin = new User();
      $admin->name ="Admin";
      $admin->email = 'admin@uangkelas.com';
      $admin->password = bcrypt('rahasia');
      $admin->save();
      $admin->attachRole($adminRole);

      $siswa = new User();
      $siswa->name ="Siswa";
      $siswa->email = 'siswa@uangkelas.com';
      $siswa->password = bcrypt('rahasia');
      $siswa->save();
      $siswa->attachRole($siswaRole);

      $sekolah = new \App\Sekolah;
      $sekolah->nama = "SMK N 8 Semarang";
      $sekolah->alamat="Jl. Pandanaran II";
      $sekolah->email="smkn8smg@sch.id";
      $sekolah->telp="(024) 7277 8888";
      $sekolah->save();

      $angkatan=new \App\Angkatan;
      $angkatan->angkatan=2018;
      $angkatan->save();

      $jur= new \App\Jurusan;
      $jur->nama="Rekayasa Perangkat Lunak";
      $jur->sekolah_id= $sekolah->id;
      $jur->save();

      $kelas=new \App\Kelas();
      $kelas->nama="XII RPL 2";
      $kelas->jurusan_id=$jur->id;
      $kelas->angkatan_id=$angkatan->id;
      $kelas->save();

      $dudi=new \App\Dudi;
      $dudi->nm_dudi="Kelas Koding";
      $dudi->alamat="Jl. jj";
      $dudi->telp="(024) 8377 4777";
      $dudi->email ="kelaskodin@gmail.com";
      $dudi->web ="kelaskodin.com";
      $dudi->kpl_dudi="mr. syntax";
      $dudi->sekolah_id=$sekolah->id;
      $dudi->save();

      $univ= new \App\Universitas;
      $univ->nm_universitas="UDINUS";
      $univ->alamat="Jl. k";
      $univ->email="hdhd";
      $univ->telp="0598869494";
      $univ->web="http://udinus.ac.id";
      $univ->save();

      $profile=new \App\Profileku;
      $profile->user_id= $siswa->id;
      $profile->kelas_id= $kelas->id;
      $profile->universitas_id= $univ->id;
      $profile->alamat="siswa";
      $profile->alamat="Jl. Ku";
      $profile->tmp_lahir="Semarang";
      $profile->tgl_lahir= date('Y-m-d');
      $profile->no_ktp=337788877666;
      $profile->nm_ibu="";
      $profile->nm_ayah="";
      $profile->nm_wali="";
      $profile->telp="029384884";
      $profile->whatsapp="+1 773 8849";
      $profile->facebook="m.dawamraja";
      $profile->twiter="";
      $profile->instagram="@dawamraja";
      $profile->pekerjaan="Full Stuck Developer";
      $profile->almt_pekerjaan="";
      $profile->pghsl_bulanan=15000000;
      $profile->photo="";
      $profile->kutipan="1 + 1 = 10";
      $profile->save();

      $profile=new \App\Profileku;
      $profile->user_id= $admin->id;
      $profile->universitas_id= $univ->id;
      $profile->alamat="Jl. Ku";
      $profile->tmp_lahir="Semarang";
      $profile->tgl_lahir= date('Y-m-d');
      $profile->no_ktp=337788877666;
      $profile->nm_ibu="";
      $profile->nm_ayah="";
      $profile->nm_wali="";
      $profile->telp="029384884";
      $profile->whatsapp="+1 773 8849";
      $profile->facebook="m.dawamraja";
      $profile->twiter="";
      $profile->instagram="@dawamraja";
      $profile->pekerjaan="Full Stuck Developer";
      $profile->almt_pekerjaan="";
      $profile->pghsl_bulanan=15000000;
      $profile->photo="";
      $profile->kutipan="1 + 1 = 10";
      $profile->save();
    
      $addAdmin = new \App\Admin;
      $addAdmin->user_id = $admin->id;
      $addAdmin->sekolah_id = $sekolah->id;
      $addAdmin->save();

      $magang=new \App\Magang;
      $magang->siswa_id=$siswa->id;
      $magang->dudi_id=$dudi->id;
      $magang->angkatan_id=$angkatan->id;
      $magang->magang=6;
      $magang->save();

      \DB::table('universitas')->insert([
       
        ['nm_universitas' => 'Universitas Malikussaleh, Lhokseumawe'],
        ['nm_universitas' => 'Politeknik Negeri Lhokseumawe, Lhokseumawe'],
        ['nm_universitas' => 'Politeknik Negeri Aceh, Banda Aceh'],
        ['nm_universitas' => 'Universitas Samudra, Langsa'],

        ['nm_universitas' => 'IAIN Raden Fatah'],
        ['nm_universitas' => 'Universitas Lampung, Bandar Lampung'],
        ['nm_universitas' => 'Politeknik Negeri Lampung, Bandar Lampung'],
        ['nm_universitas' => 'Poltekkes Kemenkes Tanjungkarang, Bandar Lampung'],
        ['nm_universitas' => 'STIM (Sekolah Tinggi Olahraga Metro), Kota Metro'],
        ['nm_universitas' => 'Institut Agama Islam Negeri Raden Intan, Bandar Lampung'],
        ['nm_universitas' => 'STAIN Jurai Siwo Metro'],
        ['nm_universitas' => 'IAIN Raden Intan, Bandar Lampung'],
        ['nm_universitas' => 'Universitas Bangka Belitung, Bangka Belitung'],
        ['nm_universitas' => 'Politeknik Manufaktur, Bangka Belitung'],
        ['nm_universitas' => 'Poltekkes Kemenkes Pangkal Pinang, Bangka Belitung'],
        ['nm_universitas' => 'STAIN Syekh Abdurrahman Siddik, Bangka Belitung'],
        ['nm_universitas' => 'Universitas Sultan Ageng Tirtayasa, Serang'],
        ['nm_universitas' => 'Perguruan Tinggi Buddhi, Karawaci'],
        ['nm_universitas' => 'Universitas Negeri Semarang, Semarang'],
        ['nm_universitas' => 'Universitas Jenderal Soedirman, Purwokerto'],
        ['nm_universitas' => 'Universitas Negeri Surakarta Sebelas Maret, Surakarta'],
        ['nm_universitas' => 'Politeknik Negeri Semarang, Semarang '],
        ['nm_universitas' => 'Politeknik Maritim Negeri Indonesia, Semarang'],
        ['nm_universitas' => 'Institut Seni Indonesia Surakarta, Surakarta (ISI Solo, dahulu STSI)'],
        ['nm_universitas' => 'Universitas Tidar Magelang, Magelang'],
        ['nm_universitas' => 'UIN Walisongo, Semarang'],
        ['nm_universitas' => 'IAIN Surakarta, Surakarta'],
        ['nm_universitas' => 'STAIN Kudus, Kudus'],
        ['nm_universitas' => 'STAIN Pekalongan, Pekalongan'],
        ['nm_universitas' => 'IAIN Salatiga, Salatiga'],
        
     ]);

    }
}
