    <!-- MODAL -->
    <div class="row">
        <div class="col-md-12">

        <div id="modal-addDudi" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-full">
                <div class="modal-content p-0 b-0">
                    <div class="panel panel-color panel-primary">
                        <div class="panel-heading"> 
                            <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h3 class="panel-title tetx-center">Tambah DUDI</h3> 
                        </div> 
                        <div class="panel-body"> 
                            <!-- table  -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box table-responsive">

                                        <table class="table dt-responsive nowrap" cellspacing="0"
                                                width="100%">
                                            <thead>
                                            <tr>
                                                <th>Nama Dudi</th>
                                                <th>Email</th>
                                                <th>Telp</th>
                                                <th>Alamat</th>
                                                <th>Website</th>
                                                <th>HRD</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <form action="/admin/api/addDudi" method="post" class="form">
                                                {{csrf_field()}}
                                                @for($i=0;$i< 5;$i++)
                                                <tr>
                                                    <td>
                                                   
                                                        <input type="hidden" name="sekolah_id" value="{{$sekolah_id}}">
                                                        <input type="text"name="nm_dudi[]" class="form-control" placeholder="Nama DUDI">
                                                    </td>
                                                    <td>
                                                        <input type="email"name="email[]" class="form-control" placeholder="Email">
                                                    </td>
                                                    <td>
                                                        <input type="number"name="telp[]" class="form-control" placeholder="Telp.">
                                                    </td>
                                                    <td>
                                                        <input type="text"name="alamat[]" class="form-control" placeholder="Alamat">
                                                    </td>
                                                    <td>
                                                        <input type="url"name="website[]" class="form-control" placeholder="Website">
                                                    </td>
                                                    <td>
                                                        <input type="text"name="hrd[]" class="form-control" placeholder="HRD">
                                                    </td>
                                                </tr>
                                                @endfor
                                                <tr>
                                                    <td colspan="2"></td>
                                                    <td colspan="2">
                                                    <div class="row">   
                                                        <div class="col-md-12">
                                                            <button class="btn btn-primary col-sm-12" type="submit">Tambah</button>
                                                        </div>
                                                    </div>
                                                    </td>
                                                </tr>
                                            </form>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- endtable -->
                        </div> 
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div id="modal-addMagang" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-full">
                <div class="modal-content p-0 b-0">
                    <div class="panel panel-color panel-primary">
                        <div class="panel-heading"> 
                            <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h3 class="panel-title tetx-center">Input Siswa Magang</h3> 
                        </div> 
                        <div class="panel-body"> 
                            <!-- table  -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box table-responsive">
                                    <form action="/admin/api/addMagang" method="post" class="form">
                                                {{csrf_field()}}
                                        <table class="table dt-responsive nowrap" cellspacing="0"
                                                width="100%">
                                            <thead>
                                            <tr>
                                                <th>Nama Siswa</th>
                                                <th>Tempat Magang</th>
                                                <th>Lama Magang</th>
                                                <th>Tahun Ajaran</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @for($i=0;$i< 5;$i++)
                                                <tr>
                                                    <td>
                                                   <!-- Auto Complate Belum -->
                                                        <input type="hidden" name="sekolah_id" value="{{$sekolah_id}}">
                                                        <input type="text"  name="nm_siswa[]" class="typeahead  form-control" placeholder="Nama Siswa">
                                                    </td>
                                                    <td>
                                                    <!-- Auto Complate Belum -->
                                                        <input type="text" name="dudi[]" class="typeahead2 form-control" placeholder="Tempat Magang">
                                                    </td>
                                                    <td>
                                                        <select name="lama[]" id="" data-style="btn-primary" class="form-control selectpicker show-tick">
                                                            <option value="3"> 3 Bulan</option>
                                                            <option value="4"> 4 Bulan</option>
                                                            <option value="6"> 6 Bulan</option>
                                                            <option value="12"> 12 Bulan</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="angkatan_id[]" class="form-control selectpicker show-tick" data-style="btn-purple"  >
                                                            <?php
                                                            $angkatan = \App\Angkatan::get();
                                                            ?>
                                                            @foreach($angkatan as $data)
                                                            <option value="{{$data->id}}">{{$data->angkatan}} - {{($data->angkatan + 1)}}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                </tr>
                                                @endfor
                                                <tr>
                                                    <td colspan="2"></td>
                                                    <td colspan="2">
                                                    <div class="row">   
                                                        <div class="col-md-12">
                                                            <button class="btn btn-primary col-sm-12" type="submit">Tambah</button>
                                                        </div>
                                                    </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- endtable -->
                        </div> 
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        </div>
    </div>


                        