<div class="row">
    <div class="col-md-12">

        <div id="modal-token" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg"> 
                <div class="modal-content"> 
                    <div class="panel panel-color panel-primary">
                        <div class="panel-heading"> 
                            <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h3 class="panel-title text-center">Token {{$sekolah->nama}}</h4> 
                        </div> 
                        <div class="panel-body"> 
                            <!-- table  -->
                            <div class="row">
                                
                                <div class="col-md-12"> 
                                    <ul class="nav nav-tabs tabs">
                                        <li class="active tab">
                                            <a href="#tab-token" data-toggle="tab" aria-expanded="false"> 
                                                <span class="visible-xs"><i class="fa fa-home"></i></span> 
                                                <span class="hidden-xs">Token</span> 
                                            </a> 
                                        </li> 
                                        
                                    </ul> 
                                    <div class="tab-content"> 
                                        <div class="tab-pane active" id="tab-token"> 

                                            <!-- table  -->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box table-responsive">
                                                       <h1 class="text-center">{{$sekolah->token}}</h1>
                                                       <hr>
                                                       <span class="text-center"> <i>*Aktif sd {{\Carbon\Carbon::parse($sekolah->exp_token)->format('l, d F Y')}}</i> </span>    
                                                    </div>
                                                    <form action="/admin/api/generateToken" method="post">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="sekolah_id" value="{{$sekolah->id}}">
                                                        <center><button type="submit" class="btn btn-primary">Generate Token</button></center>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- endtable -->
                                        </div> 
                                        
                                    </div> 
                                </div> 
                            </div>
                            <!-- endtable -->
                        </div> 
                    </div>
                </div> 
            </div>
        </div><!-- /.modal -->

        <div id="modal-jurusan" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg"> 
                <div class="modal-content"> 
                    <div class="panel panel-color panel-primary">
                        <div class="panel-heading"> 
                            <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h3 class="panel-title text-center">Jurusan {{$sekolah->nama}}</h4> 
                        </div> 
                        <div class="panel-body"> 
                            <!-- table  -->
                            <div class="row">
                                
                                <div class="col-md-12"> 
                                    <ul class="nav nav-tabs tabs">
                                        <li class="active tab">
                                            <a href="#tab-jurusan" data-toggle="tab" aria-expanded="false"> 
                                                <span class="visible-xs"><i class="fa fa-home"></i></span> 
                                                <span class="hidden-xs">Jurusan</span> 
                                            </a> 
                                        </li> 
                                        <li class="tab"> 
                                            <a href="#tab-addJurusan" data-toggle="tab" aria-expanded="false"> 
                                                <span class="visible-xs"><i class="fa fa-user"></i></span> 
                                                <span class="hidden-xs">Tambah Jurusan</span> 
                                            </a> 
                                        </li> 
                                    </ul> 
                                    <div class="tab-content"> 
                                        <div class="tab-pane active" id="tab-jurusan"> 

                                                                    <!-- table  -->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card-box table-responsive">
                                            @if($message = Session::get('success1'))
                                            <div class="alert alert-success">
                                                <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                <strong>{{$message}}</strong>
                                            </div>
                                            <hr>
                                            @endif
                                            @if($message = Session::get('wrong1'))
                                            <div class="alert alert-danger">
                                                <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                <strong>{{$message}}</strong>
                                            </div>
                                            <hr>
                                            @endif                                  

                                            <table id="datatable-jurusan"
                                                    class="table  nowrap" cellspacing="0"
                                                    width="100%">
                                                <thead>
                                                <tr>
                                                    <th>Nama Jurusan</th>
                                                    <th>Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                    @foreach($jurusan as $data)
                                                    <tr> 
                                                        <td>{{$data->nama}}</td>
                                                        <td>
                                                            <form action="/admin/api/{{$data->id}}/delJurusan" method="post">
                                                                {{csrf_field()}}
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-sm btn-warning waves-effect"><i class="md-edit"></i> Edit</button>
                                                                    <button type="submit" class="btn btn-sm btn-danger waves-effect"><i class="md-delete"> </i> Hapus</button>
                                                                </div>
                                                            </form>
                                                            
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- endtable -->

                                        </div> 
                                        <div class="tab-pane" id="tab-addJurusan">
                                            <!-- table  -->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box table-responsive">

                                                        <table class="table dt-responsive nowrap" cellspacing="0"
                                                                width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th>Nama Jurusan</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                <form action="/admin/api/addJurusan" method="post" class="form">
                                                                {{csrf_field()}}
                                                                @for($i=0;$i< 3;$i++)
                                                                <tr>
                                                                    <td>
                                                                        <div class="form-group col-sm-12">
                                                                        <input type="hidden" name="sekolah_id" value="{{$sekolah_id}}">
                                                                        <input type="text" name="jurusan[]" class="form-control" style="width:100%" placeholder="Nama Jurusan">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                @endfor
                                                                <tr>
                                                                    <td><button class="btn btn-primary" type="submit">Tambah</button></td>
                                                                </tr>
                                                                </form>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- endtable -->
                                        </div> 
                                    </div> 
                                </div> 

                            </div>
                            <!-- endtable -->
                        </div> 
                    </div>
                </div> 
            </div>
        </div><!-- /.modal -->

        <div id="modal-kelas" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg"> 
                <div class="modal-content"> 
                    <div class="panel panel-color panel-primary">
                        <div class="panel-heading"> 
                            <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h3 class="panel-title text-center">Kelas {{$sekolah->nama}}</h3> 
                        </div> 
                        <div class="panel-body"> 
                            <div class="row">
                            
                                <div class="col-md-12"> 
                                    <ul class="nav nav-tabs tabs">
                                        <li class="active tab">
                                            <a href="#tab-kelas" data-toggle="tab" aria-expanded="false"> 
                                                <span class="visible-xs"><i class="fa fa-home"></i></span> 
                                                <span class="hidden-xs">Kelas</span> 
                                            </a> 
                                        </li> 
                                        <li class="tab"> 
                                            <a href="#tab-addKelas" data-toggle="tab" aria-expanded="false"> 
                                                <span class="visible-xs"><i class="fa fa-user"></i></span> 
                                                <span class="hidden-xs">Tambah Kelas</span> 
                                            </a> 
                                        </li> 
                                    </ul> 
                                    <div class="tab-content"> 
                                        <div class="tab-pane active" id="tab-kelas"> 

                                            <!-- table  -->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box table-responsive">
                                                        @if($message = Session::get('success1'))
                                                        <div class="alert alert-success">
                                                            <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                            <strong>{{$message}}</strong>
                                                        </div>
                                                        <hr>
                                                        @endif
                                                        @if($message = Session::get('wrong1'))
                                                        <div class="alert alert-danger">
                                                            <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                            <strong>{{$message}}</strong>
                                                        </div>
                                                        <hr>
                                                        @endif                                  

                                                        <table id="datatable-kelas" class="table dt-responsive nowrap" cellspacing="0" width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th>Nama Kelas</th>
                                                                <th>Jurusan</th>
                                                                <th>Tahun Angkatan</th>
                                                                <th>Aksi</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($kelas as $data)
                                                                <tr>
                                                                    <td>{{$data->nm_kelas}}</td>
                                                                    <td>{{$data->nm_jur}}</td>
                                                                    <td>{{$data->angkatan}}</td>
                                                                    <td>
                                                                    <form action="/admin/api/{{$data->id}}/delKelas" method="post">
                                                                        {{csrf_field()}}
                                                                        <div class="btn-group">
                                                                            <button type="button" class="btn btn-sm btn-warning waves-effect"><i class="md-edit"></i> Edit</button>
                                                                            <button type="submit" class="btn btn-sm btn-danger waves-effect"><i class="md-delete"> </i> Hapus</button>
                                                                        </div>
                                                                    </form>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- endtable -->

                                        </div> 
                                        
                                        <div class="tab-pane" id="tab-addKelas">
                                            <!-- table  -->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box table-responsive">

                                                        <table class="table dt-responsive nowrap" cellspacing="0"
                                                                width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th>Nama Kelas</th>
                                                                <th>Jurusan</th>
                                                                <th>Tahun Angkatan</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <form action="/admin/api/addKelas" method="post" class="form">
                                                                {{csrf_field()}}
                                                                @for($i=0;$i< 3;$i++)
                                                                <tr>
                                                                    <td>
                                                                        <div class="form-group col-sm-12">
                                                                        <input type="text" name="nm_kelas[]" class="form-control" style="width:100%" placeholder="Nama Kelas">
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <select name="jurusan_id[]" class="form-control"  >
                                                                            <?php
                                                                            $jurusan = \App\Jurusan::where('sekolah_id',$sekolah_id)->orderBy('nama', 'Asc')->get();
                                                                            ?>
                                                                            @foreach($jurusan as $data)
                                                                            <option value="{{$data->id}}">{{$data->nama}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </td>
                                                                    <td>
                                                                        <select name="angkatan_id[]"  class="form-control" >
                                                                        <?php
                                                                        $angkatan = \App\Angkatan::get();
                                                                        ?>
                                                                        @foreach($angkatan as $data)
                                                                        <option value="{{$data->id}}">{{$data->angkatan}} - {{($data->angkatan + 1)}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    </td>
                                                                </tr>
                                                                @endfor
                                                                <tr>
                                                                    <td colspan="2"></td>
                                                                    <td>
                                                                    <div class="row">   
                                                                        <div class="col-md-12">
                                                                            <button class="btn btn-primary col-sm-12" type="submit">Tambah</button>
                                                                        </div>
                                                                    </div>
                                                                    </td>
                                                                </tr>
                                                            </form>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- endtable -->
                                        </div> 
                                    </div> 
                                </div> 
                            </div>
                        </div> 
                    </div>
                </div> 
            </div>
        </div><!-- /.modal -->

        <div id="modal-siswa" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-full"> 
                <div class="modal-content"> 
                    <div class="panel panel-color panel-primary">
                        <div class="panel-heading"> 
                            <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h3 class="panel-title text-center">Siswa {{$sekolah->nama}}</h3> 
                        </div> 
                        <div class="panel-body"> 
                            <div class="row">
                            
                                <div class="col-md-12"> 
                                    <ul class="nav nav-tabs tabs">
                                        <li class="active tab">
                                            <a href="#tab-siswa" data-toggle="tab" aria-expanded="false"> 
                                                <span class="visible-xs"><i class="fa fa-home"></i></span> 
                                                <span class="hidden-xs">Siswa</span> 
                                            </a> 
                                        </li> 
                                        <li class="tab"> 
                                            <a href="#tab-addSiswa" data-toggle="tab" aria-expanded="false"> 
                                                <span class="visible-xs"><i class="fa fa-user"></i></span> 
                                                <span class="hidden-xs">Tambah Siswa</span> 
                                            </a> 
                                        </li> 
                                    </ul> 
                                    <div class="tab-content"> 
                                        <div class="tab-pane active" id="tab-siswa"> 

                                            <!-- table  -->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box table-responsive">
                                                        @if($message = Session::get('success1'))
                                                        <div class="alert alert-success">
                                                            <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                            <strong>{{$message}}</strong>
                                                        </div>
                                                        <hr>
                                                        @endif
                                                        @if($message = Session::get('wrong1'))
                                                        <div class="alert alert-danger">
                                                            <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                            <strong>{{$message}}</strong>
                                                        </div>
                                                        <hr>
                                                        @endif                                  

                                                        <table id="datatable-siswa"
                                                                class="table dt-responsive nowrap" cellspacing="0"
                                                                width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th>Nama Siswa</th>
                                                                <th>Kelas</th>
                                                                <th>Jurusan</th>
                                                                <th>Angkatan</th>
                                                                <th>Status</th>
                                                                <th>Aksi</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($siswa as $data)
                                                                <tr>
                                                                    <td>{{$data->nm_siswa}}</td>
                                                                    <td>{{$data->nm_kelas}}</td>
                                                                    <td>{{$data->nm_jur}}</td>
                                                                    <td>{{$data->angkatan}} - {{$data->angkatan+1}}</td>
                                                                    <td>
                                                                        @if($data->status == 'siswa')
                                                                            <span class="label label-success">Siswa</span>
                                                                        @else
                                                                            <span class="label label-danger">Alumni</span>
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                    <form action="/admin/api/{{$data->id}}/delSiswa" method="post">
                                                                        {{csrf_field()}}
                                                                        <div class="btn-group">
                                                                            <button type="button" class="btn btn-sm btn-warning waves-effect"><i class="md-edit"></i> Edit</button>
                                                                            <button type="button" class="btn btn-sm btn-primary waves-effect"><i class="md-more-horiz"></i> Detail</button>
                                                                            <button type="submit" class="btn btn-sm btn-danger waves-effect"><i class="md-delete"> </i> Hapus</button>
                                                                        </div>
                                                                    </form>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- endtable -->

                                        </div> 
                                        <div class="tab-pane" id="tab-addSiswa">
                                            <!-- table  -->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box table-responsive">
                                                    <form action="/admin/api/addSiswa" method="post" class="form">
                                                                {{csrf_field()}}
                                                        <table id="datatable-addSiswa" class="table dt-responsive nowrap" cellspacing="0"
                                                                width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Nama Siswa</th>
                                                                <th>Kelas</th>
                                                                <th>Tahun Angkatan</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                @for($i=0;$i< 10;$i++)
                                                                <tr>
                                                                    <td>{{$i+1}}</td>
                                                                    <td>
                                                                        <div class="form-group col-sm-12">
                                                                        <input type="text"name="nm_siswa[]" class="form-control" style="width:100%" placeholder="Nama Siswa">
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                    <select class="form-control selectpicker show-tick" data-style="btn-white" name="nm_kelas[]" class="form-control">
                                                                        <?php
                                                                        $kelas = \App\Jurusan::where('sekolah_id',$sekolah_id)
                                                                                ->join('kelas','kelas.jurusan_id','jurusans.id')
                                                                                ->select('kelas.id as id','kelas.nama as nama')
                                                                                ->orderBy('nama', 'Asc')->get();
                                                                        ?>
                                                                        @foreach($kelas as $data)
                                                                        <option value="{{$data->nama}}">{{$data->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    </td>
                                                                    <td>
                                                                        <select class="form-control selectpicker show-tick" data-style="btn-white" name="angkatan_id[]" class="form-control">
                                                                        <?php
                                                                        $angkatan = \App\Angkatan::get();
                                                                        ?>
                                                                        @foreach($angkatan as $data)
                                                                        <option value="{{$data->id}}">{{$data->angkatan}} - {{($data->angkatan + 1)}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    </td>
                                                                </tr>
                                                                @endfor
                                                            </tbody>
                                                        </table>
                                                        <div class="row"> 
                                                            <div class="col-md-4">
                                                                <select class="form-control selectpicker show-tick" data-style="btn-danger btn-custom" class="form-control" name="status" id="">
                                                                    <option value="siswa">Belum Lulus</option>
                                                                    <option value="alumni">Lulus</option>
                                                                </select>
                                                                <br>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <button class="btn btn-primary col-sm-12" type="submit">Tambah</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- endtable -->
                                        </div> 
                                    </div> 
                                </div> 
                            </div>
                        </div> 
                    </div>
                </div> 
        </div><!-- /.modal -->

        
    </div>
</div>