@extends('layouts.master')
@section('title')
<title>Ubold - Responsive Admin Dashboard Template</title>
@endsection
@section('css')
<link href="/ubold/assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />

@endsection
@section('content')
<br>
<div class="content-page">
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">

                <h4 class="page-title">Management PKL</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="/">Home</a>
                    </li>
                    <li>
                        <a href="/owner">Dashboard</a>
                    </li>
                    <li class="active">
                        Manage Magang 
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                    <div class="portlet">
                        <div class="portlet-heading bg-primary">
                            <h3 class="portlet-title">
                                Data Siswa Magang
                            </h3>
                            <div class="portlet-widgets">
                                <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                <span class="divider"></span>
                                <a data-toggle="collapse" data-parent="#accordion1" href="#data-dudi"><i class="ion-minus-round"></i></a>
                                <span class="divider"></span>
                                <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div id="data-dudi" class="panel-collapse collapse in">
                            <div class="portlet-body">
                                <!-- table  -->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card-box table-responsive">
                                            @if($message = Session::get('success1'))
                                            <div class="alert alert-success">
                                                <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                <strong>{{$message}}</strong>
                                            </div>
                                            <hr>
                                            @endif
                                            @if($message = Session::get('wrong1'))
                                            <div class="alert alert-danger">
                                                <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                <strong>{{$message}}</strong>
                                            </div>
                                            <hr>
                                            @endif

                                            <h4 class="m-t-0 header-title">
                                                <button class="btn btn-info waves-effect waves-light" data-toggle="modal" data-target="#modal-addMagang"> Tambah Siswa Magang</button>
                                            </h4>                                     

                                            <table id="datatable-magang"
                                                    class="table dt-responsive nowrap" cellspacing="0"
                                                    width="100%">
                                                <thead>
                                                <tr>
                                                    <th>Nama Siswa</th>
                                                    <th>Tempat Magang</th>
                                                    <th>Lama Magang</th>
                                                    <th>Kelas</th>
                                                    <th>Tahun Ajaran</th>
                                                    <th>Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $magang = \App\Magang::join('users','magangs.siswa_id','users.id')
                                                                    ->join('profilekus','users.id','profilekus.user_id')
                                                                    ->join('kelas','profilekus.kelas_id','kelas.id')
                                                                    ->join('angkatans','magangs.angkatan_id','angkatans.id')
                                                                    ->join('dudis','magangs.dudi_id','dudis.id')
                                                                    ->select('magangs.id as id','users.name as nm_siswa','magangs.magang as magang','angkatans.angkatan as angkatan','kelas.nama as nm_kelas','dudis.nm_dudi as nm_dudi')
                                                                    ->get();
                                                ?>
                                                @foreach($magang as $data)
                                                <tr>
                                                    <td>{{$data->nm_siswa}}</td>
                                                    <td>{{$data->nm_dudi}}</td>
                                                    <td>{{$data->magang}}</td>
                                                    <td>{{$data->nm_kelas}}</td>
                                                    <td>{{$data->angkatan}}</td>
                                                    <td>
                                                        <form action="/admin/api/{{$data->id}}/delDudi" method="post">
                                                        {{csrf_field()}}
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-sm btn-warning waves-effect"><i class="md-edit"></i> Edit</button>
                                                                <button type="button" class="btn btn-sm btn-primary waves-effect"><i class="md-visibility"></i> Detail</button>
                                                                <button type="submit" class="btn btn-sm btn-danger waves-effect"><i class="md-delete"> </i> Hapus</button>
                                                            </div>
                                                        </form>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            
                                        </div>
                                    </div>
                                </div>
                                <!-- endtable -->
                            </div>
                        </div>
                    </div>
            </div>
        </div>            
    </div>
</div>
</div>
@include('admin.modal-pkl')
@endsection

@section('js')
<script src="/ubold/assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<script type="text/javascript">
    var path = "/admin/api/searchSiswa";
    $('input.typeahead').typeahead({
        source:  function (query, process) {
        return $.get(path, { query: query }, function (data) {
                return process(data);
            });
        }
    });
    var path2 = "/admin/api/searchDudi";
    $('input.typeahead2').typeahead({
        source:  function (query, process) {
        return $.get(path2, { query: query }, function (data) {
                return process(data);
            });
        }
    });
</script>
@endsection