@extends('layouts.master')
@section('title')
<title>Ubold - Responsive Admin Dashboard Template</title>
@endsection
@section('css')

@endsection
@section('content')
<br>
<div class="content-page">
<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">

                <h4 class="page-title">Management PKL</h4>
                <ol class="breadcrumb">
                    <li>
                        <a href="/">Home</a>
                    </li>
                    <li>
                        <a href="/owner">Dashboard</a>
                    </li>
                    <li class="active">
                        Manage Dudi 
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                    <div class="portlet">
                        <div class="portlet-heading bg-primary">
                            <h3 class="portlet-title">
                                Data Dudi
                            </h3>
                            <div class="portlet-widgets">
                                <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                <span class="divider"></span>
                                <a data-toggle="collapse" data-parent="#accordion1" href="#data-dudi"><i class="ion-minus-round"></i></a>
                                <span class="divider"></span>
                                <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div id="data-dudi" class="panel-collapse collapse in">
                            <div class="portlet-body">
                                <!-- table  -->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card-box table-responsive">
                                            @if($message = Session::get('success1'))
                                            <div class="alert alert-success">
                                                <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                <strong>{{$message}}</strong>
                                            </div>
                                            <hr>
                                            @endif
                                            @if($message = Session::get('wrong1'))
                                            <div class="alert alert-danger">
                                                <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                <strong>{{$message}}</strong>
                                            </div>
                                            <hr>
                                            @endif

                                            <h4 class="m-t-0 header-title">
                                                <button class="btn btn-info waves-effect waves-light" data-toggle="modal" data-target="#modal-addDudi"> Tambah Dudi</button>
                                            </h4>                                     

                                            <table id="datatable-dudi"
                                                    class="table dt-responsive nowrap" cellspacing="0"
                                                    width="100%">
                                                <thead>
                                                <tr>
                                                    <th>Nama Dudi</th>
                                                    <th>Email Dudi</th>
                                                    <th>Telp. Dudi</th>
                                                    <th>Alamat Dudi</th>
                                                    <th>Website</th>
                                                    <th>Nama HRD</th>
                                                    <th>Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $dudi = \App\Dudi::where('sekolah_id', $sekolah_id)->get();
                                                ?>
                                                @foreach($dudi as $data)
                                                <tr>
                                                    <td>{{$data->nm_dudi}}</td>
                                                    <td>{{$data->email}}</td>
                                                    <td>{{$data->telp}}</td>
                                                    <td>{{$data->alamat}}</td>
                                                    <td><a href="{{$data->web}}">{{$data->web}}</a></td>
                                                    <td>{{$data->kpl_dudi}}</td>
                                                    <td>
                                                        <form action="/admin/api/{{$data->id}}/delDudi" method="post">
                                                        {{csrf_field()}}
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-sm btn-warning waves-effect"><i class="md-edit"></i> Edit</button>
                                                                <button type="button" class="btn btn-sm btn-primary waves-effect"><i class="md-visibility"></i> Detail</button>
                                                                <button type="submit" class="btn btn-sm btn-danger waves-effect"><i class="md-delete"> </i> Hapus</button>
                                                            </div>
                                                        </form>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            
                                        </div>
                                    </div>
                                </div>
                                <!-- endtable -->
                            </div>
                        </div>
                    </div>
            </div>
        </div>            
    </div>
</div>
</div>
@include('admin.modal-pkl')
@endsection

@section('js')

@endsection