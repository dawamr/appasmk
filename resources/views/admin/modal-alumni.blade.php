<div class="row">
    <div class="col-md-12">

        <div id="modal-univ" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-full"> 
                <div class="modal-content"> 
                    <div class="panel panel-color panel-primary">
                        <div class="panel-heading"> 
                            <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h3 class="panel-title text-center">Data Universitas</h3> 
                        </div> 
                        <div class="panel-body"> 
                            <div class="row">
                            
                                <div class="col-md-12"> 
                                    <ul class="nav nav-tabs tabs">
                                        <li class="active tab">
                                            <a href="#tab-univ" data-toggle="tab" aria-expanded="false"> 
                                                <span class="visible-xs"><i class="fa fa-home"></i></span> 
                                                <span class="hidden-xs">Universitas</span> 
                                            </a> 
                                        </li> 
                                        <li class="tab"> 
                                            <a href="#tab-addUniv" data-toggle="tab" aria-expanded="false"> 
                                                <span class="visible-xs"><i class="fa fa-user"></i></span> 
                                                <span class="hidden-xs">Tambah Universitas</span> 
                                            </a> 
                                        </li> 
                                    </ul> 
                                    <div class="tab-content"> 
                                        <div class="tab-pane active" id="tab-univ"> 

                                            <!-- table  -->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box table-responsive">

                                                        <table id="datatable-universitas" class="table dt-responsive nowrap" cellspacing="0"
                                                                width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th>Nama Universitas</th>
                                                                <th>Alamat</th>
                                                                <th>Email</th>
                                                                <th>Telp</th>
                                                                <th>Website</th>
                                                                <th>Aksi</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            $univ = \App\Universitas::orderBy('nm_universitas','Asc')->get();
                                                            ?>
                                                            @foreach($univ as $data)
                                                            <tr>
                                                                <td>{{$data->nm_universitas}}</td>
                                                                <td>{{$data->alamat}}</td>
                                                                <td>{{$data->email}}</td>
                                                                <td>{{$data->telp}}</td>
                                                                <td>{{$data->web}}</td>
                                                                <td></td>
                                                            </tr>

                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- endtable -->

                                        </div> 
                                        <div class="tab-pane" id="tab-addUniv">
                                            <!-- table  -->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box table-responsive">
                                                    <form action="/admin/api/addUniversitas" method="post" class="form">
                                                                {{csrf_field()}}
                                                        <table id="datatable-addUniv" class="table dt-responsive nowrap" cellspacing="0"
                                                                width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th>Nama Universitas</th>
                                                                <th>Alamat</th>
                                                                <th>Email</th>
                                                                <th>Telp</th>
                                                                <th>Website</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                
                                                                @for($i=0; $i< 5; $i++)
                                                                <tr>
                                                                    <td><input type="text" name="nm_univ[]" id="" class="form-control"></td>
                                                                    <td><input type="text" name="alamat[]" id="" class="form-control"></td>
                                                                    <td><input type="text" name="email[]" id="" class="form-control"></td>
                                                                    <td><input type="text" name="telp[]" id="" class="form-control"></td>
                                                                    <td><input type="text" name="web[]" id="" class="form-control"></td>
                                                                </tr>
                                                                @endfor
                                                                
                                                            </tbody>
                                                        </table>
                                                        <div class="row"> 
                                                            
                                                            <div class="col-md-12">
                                                                <br>
                                                                <button class="btn btn-primary col-sm-12" type="submit">Tambah</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- endtable -->
                                        </div> 
                                    </div> 
                                </div> 
                            </div>
                        </div> 
                    </div>
                </div> 
            </div>
        </div><!-- /.modal -->

        <div id="modal-alumni" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-full"> 
                <div class="modal-content"> 
                    <div class="panel panel-color panel-primary">
                        <div class="panel-heading"> 
                            <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h3 class="panel-title text-center">Alumni {{$sekolah->nama}}</h3> 
                        </div> 
                        <div class="panel-body"> 
                            <div class="row">
                                <div class="col-md-12"> 
                                    <ul class="nav nav-tabs tabs">
                                        <li class="active tab">
                                            <a href="#tab-alumni" data-toggle="tab" aria-expanded="false"> 
                                                <span class="visible-xs"><i class="fa fa-home"></i></span> 
                                                <span class="hidden-xs">Data Alumni</span> 
                                            </a> 
                                        </li> 
                                        <li class="tab"> 
                                            <a href="#tab-addAlumni" data-toggle="tab" aria-expanded="false"> 
                                                <span class="visible-xs"><i class="fa fa-user"></i></span> 
                                                <span class="hidden-xs">Tambah Alumni</span> 
                                            </a> 
                                        </li> 
                                    </ul> 
                                    <div class="tab-content"> 
                                        <div class="tab-pane active" id="tab-alumni"> 

                                            <!-- table  -->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box table-responsive">
                                                        @if($message = Session::get('success1'))
                                                        <div class="alert alert-success">
                                                            <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                            <strong>{{$message}}</strong>
                                                        </div>
                                                        <hr>
                                                        @endif
                                                        @if($message = Session::get('wrong1'))
                                                        <div class="alert alert-danger">
                                                            <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                            <strong>{{$message}}</strong>
                                                        </div>
                                                        <hr>
                                                        @endif                                  

                                                        <table id="datatable-alumni"
                                                                class="table dt-responsive nowrap" cellspacing="0"
                                                                width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Nama Siswa</th>
                                                                <th>Kelas</th>
                                                                <th>Tahun Ajaran</th>
                                                                <th>Universitas</th>
                                                                <th>Alamat</th>
                                                                <th>Tempat Lahir</th>
                                                                <th>Tanggal Lahir</th>
                                                                <th>No KTP</th>
                                                                <th>Nama Ayah</th>
                                                                <th>Nama Ibu</th>
                                                                <th>Nama Wali</th>
                                                                <th>Telp</th>
                                                                <th>WhatsApp</th>
                                                                <th>Facebook</th>
                                                                <th>Instagram</th>
                                                                <th>Twitter</th>
                                                                <th>Pekerjaan</th>
                                                                <th>Alamat Bekerja</th>
                                                                <th>Penghasilan (/bln)</th>
                                                                <th>Kutipan</th>
                                                                <th>Aksi</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                    $alumni = \App\User::join('role_user','users.id','role_user.user_id')
                                                                                        ->where('role_id', 3)
                                                                                        ->join('profilekus','users.id','profilekus.user_id')
                                                                                        ->where('status','alumni')
                                                                                        ->join('kelas','profilekus.kelas_id','kelas.id')
                                                                                        ->join('angkatans','kelas.angkatan_id','angkatans.id')
                                                                                        ->select('users.name as nm_siswa','kelas.nama as nm_kelas','angkatans.angkatan as angkatan','profilekus.*')
                                                                                        ->get();
                                                                        $no=1;$id=0;
                                                                ?>
                                                                @foreach($alumni as $data)
                                                                <tr>
                                                                    
                                                                    <td>{{$no++}}</td>
                                                                    <td>{{$data->nm_siswa}} @if($data->nm_siswa ==null) Data Tidak Tercantum @endif</td>
                                                                    <td>{{$data->nm_kelas}} @if($data->nm_kelas ==null) Data Tidak Tercantum @endif</td>
                                                                    <td>{{$data->angkatan}} - {{$data->angkatan+1}} @if($data->angkatan ==null) Data Tidak Tercantum @endif</td>
                                                                    <?php 
                                                                        $uv = \App\Universitas::where('id',$data->universitas_id)->select('nm_universitas as nm_univ')->first();
                                                                    ?>
                                                                    <td>{{$uv['nm_univ']}} @if($uv ==null) <span class="text-warning">Data Tidak Tercantum</span> @endif</td>
                                                                    <td>{{$data->alamat}} @if($data->alamat ==null) <span class="text-warning">Data Tidak Tercantum</span> @endif</td>
                                                                    <td>{{$data->tmp_lahir}} @if($data->tmp_lahir ==null) <span class="text-warning">Data Tidak Tercantum</span> @endif</td>
                                                                    <td>{{$data->tgl_lahir}} @if($data->tgl_lahir ==null) <span class="text-warning">Data Tidak Tercantum</span> @endif</td>
                                                                    <td>{{$data->no_ktp}} @if($data->no_ktp ==null) <span class="text-warning">Data Tidak Tercantum</span> @endif</td>
                                                                    <td>{{$data->nm_ayah}} @if($data->nm_ayah ==null) <span class="text-warning">Data Tidak Tercantum</span> @endif</td>
                                                                    <td>{{$data->nm_ibu}} @if($data->nm_ibu ==null) <span class="text-warning">Data Tidak Tercantum</span> @endif</td>
                                                                    <td>{{$data->nm_wali}} @if($data->nm_wali ==null) <span class="text-warning">Data Tidak Tercantum</span> @endif</td>
                                                                    <td>{{$data->telp}} @if($data->telp ==null) <span class="text-warning">Data Tidak Tercantum</span> @endif</td>
                                                                    <td>{{$data->whatsapp}} @if($data->whatsapp ==null) <span class="text-warning">Data Tidak Tercantum</span> @endif</td>
                                                                    <td>{{$data->facebook}} @if($data->facebook ==null) <span class="text-warning">Data Tidak Tercantum</span> @endif</td>
                                                                    <td>{{$data->twiter}} @if($data->twiter ==null) <span class="text-warning">Data Tidak Tercantum</span> @endif</td>
                                                                    <td>{{$data->instagram}} @if($data->instagram ==null) <span class="text-warning">Data Tidak Tercantum</span> @endif</td>
                                                                    <td>{{$data->pekerjaan}} @if($data->pekerjaan ==null) <span class="text-warning">Data Tidak Tercantum</span> @endif</td>
                                                                    <td>{{$data->almt_pekerjaan}} @if($data->almt_pekerjaan ==null) <span class="text-warning">Data Tidak Tercantum</span> @endif</td>
                                                                    <td>{{$data->pghsl_bulanan}} @if($data->pghsl_bulanan ==null) <span class="text-warning">Data Tidak Tercantum</span> @endif</td>
                                                                    <td>{{$data->kutipan}} @if($data->kutipan ==null) <span class="text-warning">Data Tidak Tercantum</span> @endif</td>
                                                                    <td></td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- endtable -->

                                        </div> 
                                        <div class="tab-pane" id="tab-addAlumni">
                                            <!-- table  -->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box table-responsive">
                                                    <form action="/admin/api/addAlumni" method="post" class="form">
                                                                {{csrf_field()}}
                                                        <table id="datatable-addAlumni" class="table dt-responsive nowrap" cellspacing="0"
                                                                width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th>Ceklis</th>
                                                                <th>Nama Siswa</th>
                                                                <th>Kelas</th>
                                                                <th>Tahun Angkatan</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php 
                                                                    $alumni = \App\User::join('role_user','users.id','role_user.user_id')
                                                                                        ->where('role_id', 3)
                                                                                        ->join('profilekus','users.id','profilekus.user_id')
                                                                                        ->where('status','siswa')
                                                                                        ->join('kelas','profilekus.kelas_id','kelas.id')
                                                                                        ->join('angkatans','kelas.angkatan_id','angkatans.id')
                                                                                        ->select('profilekus.id as id','users.name as nm_siswa','kelas.nama as nm_kelas','angkatans.angkatan as angkatan')
                                                                                        ->get();
                                                                ?>
                                                               @foreach($alumni as $data)
                                                               <tr>
                                                                <td>
                                                                    <input type="checkbox" name="cek[]" class="form-control" id="">
                                                                </td>
                                                                <td>
                                                                {{$data->nm_siswa}}
                                                                <input type="hidden" name="alumni_id[]" value="{{$data->id}}">
                                                                </td>
                                                                <td>
                                                                {{$data->nm_kelas}}
                                                                </td>
                                                                <td>
                                                                {{$data->angkatan}}
                                                                </td>
                                                               </tr>
                                                               @endforeach
                                                            </tbody>
                                                        </table>
                                                        <div class="row"> 
                                                            <div class="col-md-4">
                                                                <select class="form-control selectpicker show-tick" data-style="btn-danger btn-custom" class="form-control" name="status" id="">
                                                                    <option value="siswa">Belum Lulus</option>
                                                                    <option value="alumi">Lulus</option>
                                                                </select>
                                                                <br>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <button class="btn btn-primary col-sm-12" type="submit">Tambah</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- endtable -->
                                        </div> 
                                    </div> 
                                </div> 
                            </div>
                        </div> 
                    </div>
                </div>
            </div> 
        </div><!-- /.modal -->
    

        

    </div>
</div>
