@extends('layouts.master')
@section('title')
<title>Ubold - Responsive Admin Dashboard Template</title>
@endsection
@section('css')
<!-- DataTables -->
<link href="/ubold/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/ubold/assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
@endsection
@section('content')
<br>
<!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">

                        <h4 class="page-title">Management Siswa</h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="/">Home</a>
                            </li>
                            <li>
                                <a href="/owner">Dashboard</a>
                            </li>
                            <li class="active">
                                Manage Siswa 
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                <div class="col-lg-12">
                        <div class="portlet">
                            <div class="portlet-heading bg-info">
                                <h3 class="portlet-title">
                                    Data Dudi
                                </h3>
                                <div class="portlet-widgets">
                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#data-dudi"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="data-dudi" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <!-- table  -->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-box table-responsive">
                                                @if($message = Session::get('success1'))
                                                <div class="alert alert-success">
                                                    <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                    <strong>{{$message}}</strong>
                                                </div>
                                                <hr>
                                                @endif
                                                @if($message = Session::get('wrong1'))
                                                <div class="alert alert-danger">
                                                    <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                    <strong>{{$message}}</strong>
                                                </div>
                                                <hr>
                                                @endif

                                                <h4 class="m-t-0 header-title">
                                                    <button class="btn btn-info waves-effect waves-light" data-toggle="modal" data-target="#modal-addDudi"> Tambah Dudi</button>
                                                </h4>                                     

                                                <table id="datatable-siswa"
                                                        class="table dt-responsive nowrap" cellspacing="0"
                                                        width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Nama Siswa</th>
                                                        <th>Kelas</th>
                                                        <th>Jurusan</th>
                                                        <th>Angkatan</th>
                                                        <th>Status</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    
                                                    <?php
                                                    //BELUM SELESAI
                                                    $siswa = \App\Kelas::join('jurusans', 'jurusans.id', '=', 'kelas.jurusan_id')->where('sekolah_id', $sekolah_id)->join('angkatans', 'angkatans.id', '=', 'kelas.angkatan_id')
                                                                        ->select('kelas.nama as nm_kelas','jurusans.nama as nm_jur', 'angkatans.angkatan as angkatan')->get();
                                                    ?>
                                                    @foreach($siswa as $data)
                                                    <tr>
                                                        <td></td>
                                                        <td>{{$data->nm_kelas}}</td>
                                                        <td>{{$data->nm_jur}}</td>
                                                        <td>{{$data->angkatan}} - {{($data->angkatan + 1)}}</td>
                                                        <td></td>
                                                        <td>
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-sm btn-warning waves-effect"><i class="md-edit"></i> Edit</button>
                                                                <button type="button" class="btn btn-sm btn-primary waves-effect"><i class="md-visibility"></i> Detail</button>
                                                                <button type="button" class="btn btn-sm btn-danger waves-effect"><i class="md-delete"> </i> Hapus</button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <!-- endtable -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>
        </div>
    </div>
    
@endsection
@include('admin.modal-pkl')
@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable').dataTable();
        $('#datatable-siswa').DataTable();
        $('#datatable-tambahDudi').DataTable();
        $('#data-kota').DataTable();
    });
</script>
<script src="/ubold/assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
@endsection