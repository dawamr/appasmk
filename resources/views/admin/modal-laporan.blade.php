<div class="row">
    <div class="col-md-12">
        <div id="modal-laporanPKL" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg"> 
                <div class="modal-content"> 
                    <div class="panel panel-color panel-primary">
                        <div class="panel-heading"> 
                            <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button> 
                            <h3 class="panel-title text-center">Laporan Prakerin</h3> 
                        </div> 
                        <div class="panel-body"> 
                            <div class="row">
                            
                                <div class="col-md-12"> 
                                    <ul class="nav nav-tabs tabs">
                                        <li class="active tab">
                                            <a href="#tab-lap1" data-toggle="tab" aria-expanded="false"> 
                                                <span class="visible-xs"><i class="fa fa-home"></i></span> 
                                                <span class="hidden-xs">Laporan Prakerin Kelas</span> 
                                            </a> 
                                        </li> 
                                        <li class="tab"> 
                                            <a href="#tab-addUniv" data-toggle="tab" aria-expanded="false"> 
                                                <span class="visible-xs"><i class="fa fa-user"></i></span> 
                                                <span class="hidden-xs">Laporan Prakerin Custom</span> 
                                            </a> 
                                        </li> 
                                    </ul> 
                                    <div class="tab-content"> 
                                        <div class="tab-pane active" id="tab-lap1"> 

                                            <!-- table  -->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box table-responsive">

                                                        <table id="datatable-laporanPkl" class="table dt-responsive nowrap" cellspacing="0"
                                                                width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th>Kelas</th>
                                                                <th>Tahun Ajaran</th>
                                                                <th>PDF</th>
                                                                <th>EXCEL</th>
                                                                <th>HTML</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php 
                                                                    $kelas = \App\Jurusan::where('sekolah_id', $sekolah_id)
                                                                            ->join('kelas','kelas.jurusan_id','jurusans.id')
                                                                            ->join('angkatans','kelas.angkatan_id','angkatans.id')
                                                                            ->select('kelas.id as id','kelas.nama as nm_kelas','angkatan')
                                                                            ->get();
                                                                ?>
                                                                @foreach($kelas as $data)
                                                                <tr>
                                                                    <td class="col-md-5"><b>{{$data->nm_kelas}}</b></td>
                                                                    <td class="col-md-4">{{$data->angkatan}} - {{$data->angkatan}}</td>
                                                                    <td class="col-md-1"><a href="/admin/api/lap1/pdf/{{$data->id}}" class="btn btn-info">Download</a></td>
                                                                    <td class="col-md-1"><a href="/admin/api/lap1/xls/{{$data->id}}" class="btn btn-primary">Download</a></td>
                                                                    <td class="col-md-1"><a href="/admin/api/lap1/html/{{$data->id}}" class="btn btn-purple">Download</a></td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- endtable -->

                                        </div> 
                                        <div class="tab-pane" id="tab-addUniv">
                                            <!-- table  -->
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card-box table-responsive">
                                                    <form action="/admin/api/addUniversitas" method="post" class="form">
                                                                {{csrf_field()}}
                                                        <table id="datatable-addUniv" class="table dt-responsive nowrap" cellspacing="0"
                                                                width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th>Nama Universitas</th>
                                                                <th>Alamat</th>
                                                                <th>Email</th>
                                                                <th>Telp</th>
                                                                <th>Website</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                
                                                                @for($i=0; $i< 5; $i++)
                                                                <tr>
                                                                    <td><input type="text" name="nm_univ[]" id="" class="form-control"></td>
                                                                    <td><input type="text" name="alamat[]" id="" class="form-control"></td>
                                                                    <td><input type="text" name="email[]" id="" class="form-control"></td>
                                                                    <td><input type="text" name="telp[]" id="" class="form-control"></td>
                                                                    <td><input type="text" name="web[]" id="" class="form-control"></td>
                                                                </tr>
                                                                @endfor
                                                                
                                                            </tbody>
                                                        </table>
                                                        <div class="row"> 
                                                            
                                                            <div class="col-md-12">
                                                                <br>
                                                                <button class="btn btn-primary col-sm-12" type="submit">Tambah</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- endtable -->
                                        </div> 
                                    </div> 
                                </div> 
                            </div>
                        </div> 
                    </div>
                </div> 
            </div>
        </div><!-- /.modal -->
    </div>
</div>