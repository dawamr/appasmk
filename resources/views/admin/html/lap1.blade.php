<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
</style>

    <table>
        <tr><td colspan="5"><h1>Laporan Magang nm_kelas</h1></td></tr>
        <tr>
            <td>No</td>
            <td>Nama Siswa</td>
            <td>Tempat Magang</td>
            <td>Lama Magang</td>
            <td>Tahun Ajaran</td>
        </tr>
        <?php
            $siswa = \App\User::join('magangs','users.id','magangs.siswa_id')
                        ->join('dudis','magangs.dudi_id','dudis.id')
                        ->join('angkatans','magangs.angkatan_id','angkatans.id')
                        ->join('profilekus','users.id','profilekus.user_id')
                        ->where('profilekus.kelas_id',$id)
                        ->select('users.name as nm_siswa','dudis.nm_dudi as nm_dudi','magang','angkatan')
                        ->get();
            $no =1;
            ?>
        
        <tr>
        @foreach($siswa as $data)
        <td>{{$no++}}</td>
        <td>{{$data->nm_siswa}}</td>
        <td>{{$data->nm_dudi}}</td>
        <td>{{$data->magang}}</td>
        <td>{{$data->angkatan}} - {{$data->angkatan +1}}</td>
        </tr>
        @endforeach
    </table>
<br />
