@extends('layouts.auth')
@section('title')
<title>APPASMK - Register</title>
@endsection
@section('content')
 
<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page">
    <div class=" card-box">
        <div class="panel-heading">
            <h3 class="text-center"> Sign Up to <strong class="text-custom">APPASMK</strong> </h3>
        </div>

        <div class="panel-body">
            <form class="form-horizontal m-t-20" method="post" action="/new-user">
                {{csrf_field()}}
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" name="name" type="text" required="" placeholder="Nama Lengkap">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" required="" name="password" placeholder="Password">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" name="token" type="text" required="" placeholder="Token Sekolah">
                        @if ($errors->has('token'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('token') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="checkbox checkbox-primary">
                            <input id="checkbox-signup" type="checkbox" checked="checked">
                            <label for="checkbox-signup">I accept <a href="#">Terms and Conditions</a></label>
                        </div>
                    </div>
                </div>

                

                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-block text-uppercase waves-effect waves-light" type="submit">
                            Register
                        </button>
                    </div>
                </div>

            </form>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 text-center">
            <p>
                Already have account?<a href="/login" class="text-primary m-l-5"><b>Sign In</b></a>
            </p>
        </div>
    </div>

</div>

@endsection
