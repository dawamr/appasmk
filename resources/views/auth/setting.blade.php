@extends('layouts.auth')
@section('title')
<title>APPASMK - Register</title>
@endsection
@section('content')
 
<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page">
    <div class=" card-box">
        <div class="panel-heading">
            <h3 class="text-center"> Sign Up to <strong class="text-custom">APPASMK</strong> </h3>
        </div>

        <div class="panel-body">
            <form class="form-horizontal m-t-20" method="post" action="/update-user">
                {{csrf_field()}}
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" name="email" type="email" required="" placeholder="{{$email}}" value="{{$email}}" disabled>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                    <select class="form-control" name="jurusan_id">
                        @php
                         $jur = \App\Jurusan::where('sekolah_id',$sekolah_id)->get();
                        @endphp
                        @foreach($jur as $data)
                            <option value="{{$data->id}}">{{$data->nama}}</option>
                        @endforeach
                    </select>
                    </div>
                </div>
                <input type="hidden" name="email2" value="{{$email}}">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-xs-12">
                            <select class="form-control" name="angkatan_id">
                                @php
                                $angkatan = \App\Angkatan::get();
                                @endphp
                                @foreach($angkatan as $data)
                                    <option value="{{$data->id}}">{{$data->angkatan}}</option>
                                @endforeach
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-xs-12">
                            <select class="form-control" name="kelas_id">
                                @php
                                $angkatan = \App\Kelas::get();
                                @endphp
                                @foreach($angkatan as $data)
                                    <option value="{{$data->id}}">{{$data->nama}}</option>
                                @endforeach
                            </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                    <select class="form-control" name="status">
                            <option value="siswa">Siswa</option>
                            <option value="alumni">Alumni</option>
                    </select>
                    </div>
                </div>                

                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-block text-uppercase waves-effect waves-light" type="submit">
                            Simpan
                        </button>
                    </div>
                </div>

            </form>

        </div>
    </div>

</div>

@endsection
