
<!doctype html>
<html dir="ltr"
      hascustombackground="true"
      bookmarkbarattached="false"
      lang="en"
      class="md">
<head>
<meta charset="utf-8">
<title>New Tab</title>
<meta name="viewport" content="width=device-width">
<style>
html {
  direction: ltr;
}

body {
  font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;
  font-size: 81.25%;
}

button {
  font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;
}
@font-face {
  font-family: 'Roboto';
  font-style: normal;
  font-weight: 400;
  src: local('Roboto'), local('Roboto-Regular'),
      url(chrome://resources/roboto/roboto-regular.woff2) format('woff2');
}

@font-face {
  font-family: 'Roboto';
  font-style: normal;
  font-weight: 500;
  src: local('Roboto Medium'), local('Roboto-Medium'),
      url(chrome://resources/roboto/roboto-medium.woff2) format('woff2');
}

@font-face {
  font-family: 'Roboto';
  font-style: normal;
  font-weight: 700;
  src: local('Roboto Bold'), local('Roboto-Bold'),
      url(chrome://resources/roboto/roboto-bold.woff2) format('woff2');
}
</style>
<style>
body {
  -webkit-font-smoothing: antialiased;
  font-size: 100%;
  margin: 0;
}

/** Typography -------------------------------------------------------------- */

.content {
  /* This is identical to the default background color. It's necessary to set it
     for the case when a theme with a background image is installed. */
  background-color: rgb(50, 54, 57);
  color: rgb(189, 193, 198);
  font-size: calc(100% - 2px);
  line-height: calc(100% + 6px);
  min-width: 240px;
}

h1 {
  color: rgb(218, 220, 224);
  font-size: calc(100% + 8px);
  font-weight: 400;
  line-height: calc(100% + 8px);
}

em {
  color: white;
  font-style: normal;
}

.learn-more-button {
  color: rgb(138, 180, 248);
  text-decoration: none;
}



/** Icon -------------------------------------------------------------------- */

.icon {
  content: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNDAgMjQwIj48ZyBvcGFjaXR5PSIuOCIgZmlsbD0iI0ZGRiI+PHBhdGggZD0iTTEyMSAwQzUzLjktLjYtLjYgNTMuOSAwIDEyMWMuNiA2NS4yIDUzLjggMTE4LjQgMTE5IDExOSA2Ny4xLjYgMTIxLjYtNTMuOSAxMjEtMTIxQzIzOS40IDUzLjggMTg2LjIuNiAxMjEgMHpNOTAuNSA1OWMuMy0uOSAxLTEuNSAyLjItMS4yIDIuMi41IDE5LjkgNC4zIDE5LjkgNC4zczM2LjgtNS42IDM4LjEtNS45YzEuMS0uMiAxLjkuNCAyLjEgMS40LjEuNCA2LjMgMjEuMyAxMS43IDM5LjVINzguM0M4My45IDc5LjYgOTAuMSA2MCA5MC41IDU5em04NS45IDEwMy4zYy0uOCAxMi4yLTEwLjcgMjIuMS0yMi45IDIyLjktMTQuMy45LTI2LjEtMTAuNC0yNi4xLTI0LjUgMC0uNyAwLTEuNC4xLTIuMS0yLS43LTQuMi0xLTYuNC0xLTIuMyAwLTQuNS40LTYuNyAxLjEuMS43LjEgMS4zLjEgMiAwIDE0LjEtMTEuOCAyNS40LTI2LjEgMjQuNS0xMi4yLS44LTIyLjEtMTAuNy0yMi45LTIyLjktLjgtMTQuMiAxMC41LTI2LjEgMjQuNS0yNi4xIDEwLjIgMCAxOSA2LjMgMjIuNyAxNS4yIDIuNy0uOCA1LjUtMS4zIDguNC0xLjMgMi44IDAgNS41LjQgOC4xIDEuMiAzLjctOC45IDEyLjQtMTUuMSAyMi43LTE1LjEgMTQuMSAwIDI1LjQgMTEuOSAyNC41IDI2LjF6bTIzLjQtMzQuM0g0Mi40Yy0uMiAwLS4zLS4zLS4xLS40IDUuMi0yLjcgMzUuNC0xNy42IDc5LTE3LjYgNDMuNyAwIDczLjUgMTQuOCA3OC42IDE3LjYuMi4xLjEuNC0uMS40eiIvPjxjaXJjbGUgY3g9IjE1MS45IiBjeT0iMTYwLjgiIHI9IjE3LjQiLz48Y2lyY2xlIGN4PSI5MC4xIiBjeT0iMTYwLjgiIHI9IjE3LjQiLz48L2c+PC9zdmc+);
  height: 120px;
  width: 120px;
}

/* Medium-sized icon on medium-sized screens. */
@media (max-height: 480px),
       (max-width: 720px) {
  .icon {
    height: 72px;
    width: 72px;
  }
}

/* Very small icon on very small screens. */
@media (max-width: 720px) {
  @media (max-width: 240px),
         (max-height: 480px) {
    .icon {
      height: 48px;
      width: 48px;
    }
  }
}

/** The "Learn more" link --------------------------------------------------- */

/* By default, we only show the inline "Learn more" link. */
.content > .learn-more-button {
  display: none;
}

/** Layout ------------------------------------------------------------------ */

/* Align the content, icon, and title to to the center. */
.content {
  margin-left: auto;
  margin-right: auto;
  max-width: 600px;
}

.icon {
  margin-left: auto;
  margin-right: auto;
}

h1 {
  text-align: center;
}


.clearer {
  clear: both;
}

/* On narrow screens, align everything to the left. */
@media (max-width: 720px) {
  .content {
    max-width: 600px !important;  /* must override the rule set by JS which
                                   * is only valid for width > 720px cases. */
    text-align: start;
  }


}

/** Paddings and margins ---------------------------------------------------- */

.bulletpoints ul {
  margin: 4px 0 0;
  padding-inline-start: 16px;
}

/* Margins of floating elements don't collapse. The margin for bulletpoints
 * will usually be provided by a neighboring element. */
.bulletpoints {
  margin: 0;
}

.bulletpoints + .bulletpoints {
  margin-inline-start: 40px;
}

.bulletpoints + .bulletpoints.too-wide {
  margin-inline-start: 0;
  margin-top: 1.5rem;
}

/* Wide screens. */
@media (min-width: 720px) {
  .icon,
  h1,
  #subtitle,
  .learn-more-button {
    margin-bottom: 1.5rem;
    margin-top: 1.5rem;
  }

  .content {
    margin-top: 40px;
    min-width: 240px;
    padding: 8px 48px 24px;
  }

  /* Snap the content box to the whole height on short screens. */
  @media (max-height: 480px) {
    html,
    body,
    .content {
      height: 100%;
    }

    .content {
      margin-bottom: 0;
      margin-top: 0;
      padding-bottom: 0;
      padding-top: 0;
    }

    .icon {
      margin-top: 0;
      padding-top: 32px;  /* Define the top offset through the icon's padding,
                           * otherwise the screen height would be 100% + 32px */
    }
  }

  /* Smaller vertical margins on very short screens. */
  @media (max-height: 320px) {
    h1,
    #subtitle,
    .learn-more-button {
      margin-bottom: 16px;
      margin-top: 16px;
    }

    .icon {
      margin-bottom: 16px;
    }
  }
}

/* Narrow screens */
@media (max-width: 720px) {
  .content {
    padding: 72px 32px;
    min-width: 176px;
  }

  .icon,
  h1,
  #subtitle,
  .learn-more-button {
    margin-bottom: 1.5rem;
    margin-top: 1.5rem;
  }

  /* The two columns of bulletpoints are moved under each other. */
  .bulletpoints + .bulletpoints {
    margin-inline-start: 0;
    margin-top: 1.5rem;
  }

  /* Smaller offsets on smaller screens. */
  @media (max-height: 600px) {
    .content {
      padding-top: 48px;
    }

    .icon,
    h1,
    #subtitle,
    .learn-more-button {
      margin-bottom: 1rem;
      margin-top: 1rem;
    }

    .bulletpoints + .bulletpoints {
      margin-top: 1rem;
    }
  }

  /* Small top offset on very small screens. */
  @media (max-height: 480px) {
    .content {
      padding-top: 32px;
    }
  }

  /* Undo the first and last elements margins. */
  .icon {
    margin-top: 0;
  }

  .learn-more-button {
    margin-bottom: 0;
  }
}

/* Very narrow screens. */
@media (max-width: 240px) {
  .content {
    min-width: 192px;
    padding-left: 24px;
    padding-right: 24px;
  }
}
</style>
<script>
// Until themes can clear the cache, force-reload the theme stylesheet.
document.write('<link id="incognitothemecss" rel="stylesheet" ' +
               'href="/css/incognito_new_tab_theme.css?' +
               Date.now() + '">');
</script>
</head>
<body>
<div class="content">
<center>
  <div class="icon" role="presentation" alt=""></div>
  <h1>Wellcome APPASMK</h1>
  <p id="subtitle">
    <span>Aplikasi Alumni Dan Prakerin SMK by </span>
    <a class="learn-more-button"
        href="https://uangkelas.com"><em>Uangkelas</em></a>
  </p>
  <div>
    <div class="bulletpoints">You Have Account <a style="text-decoration:none;color: rgb(138, 180, 248)" href="/login">Login </a>|| Create New User <a style="text-decoration:none;color: rgb(138, 180, 248)" href="/register">Register</a></a>
        <!-- <ul>
          <li>Your browsing history
          <li>Cookies and site data
          <li>Information entered in forms
        </ul> -->
    </div>
    <div class="bulletpoints">

    </div>
    <br><hr>
    <p id="subtitle">
        <span><em>Copyright</em> <a style="color: rgb(138, 180, 248)" href="http://uangkelas.com">Uangkelas</a></span>
    </p>
    <div class="clearer"></div>
  </div>
</center>
</div>
<script src="/js/cr.js"></script>
<script src="/js/util.js"></script>
<script>
function recomputeLayoutWidth() {
  var bulletpoints = document.querySelectorAll('.bulletpoints');
  var content = document.querySelector('.content');

  // Unless this is the first load of the Incognito NTP in this session and
  // with this font size, we already have the maximum content width determined.
  var fontSize = window.getComputedStyle(document.body).fontSize;
  var maxWidth = localStorage[fontSize] ||
      (bulletpoints[0].offsetWidth + bulletpoints[1].offsetWidth +
       40 /* margin */ + 2 /* offsetWidths may be rounded down */);

  // Save the data for quicker access when the NTP is reloaded. Note that since
  // we're in the Incognito mode, the local storage is ephemeral and the data
  // will be discarded when the session ends.
  localStorage[fontSize] = maxWidth;

  // Limit the maximum width to 600px. That might force the two lists
  // of bulletpoints under each other, in which case we must swap the left
  // and right margin.
  var MAX_ALLOWED_WIDTH = 600;
  var tooWide = maxWidth > MAX_ALLOWED_WIDTH;
  bulletpoints[1].classList.toggle('too-wide', tooWide);
  if (tooWide)
    maxWidth = MAX_ALLOWED_WIDTH;

  content.style.maxWidth = maxWidth + 'px';
}

window.addEventListener('load', recomputeLayoutWidth);

// Handle the bookmark bar, theme, and font size change requests
// from the C++ side.
var ntp = {
  /** @param {string} attached */
  setBookmarkBarAttached: function(attached) {
    document.documentElement.setAttribute('bookmarkbarattached', attached);
  },

  /** @param {!{hasCustomBackground: boolean}} themeData */
  themeChanged: function(themeData) {
    document.documentElement.setAttribute(
        'hascustombackground', themeData.hasCustomBackground);
    $('incognitothemecss').href =
        '/css/ncognito_new_tab_theme.css?' + Date.now();
  },

  defaultFontSizeChanged: function() {
    setTimeout(recomputeLayoutWidth, 100);
  }
};
</script>
</body>
</html>
