@extends('layouts.siswa')
@section('title')
<title>APPASMK - Profile {{$user->name}}</title>
@endsection
@section('css')
    <style>
        .bg-facebook{
           background: #3b5998 !important;
        }
        .bg-twitter{
            background: #00aced !important;
        }
        .btn-instagram {
            color: #ffffff !important;
            background-color: #ff0084 !important;
        }
        .bg-instagram {
            background-color: #ff0084 !important;
        }
        .code:hover{
            color :#ff8800  !important;
        }
    </style>
@endsection
@section('content')
<br>
<div class="content-page">
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">

                    <h4 class="page-title">PROFILE PLUS</h4>
                    <ol class="breadcrumb">
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li>
                            <a href="/siswa">Dashboard</a>
                        </li>
                        <li class="active">
                            Profile Siswa
                        </li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                        <div class="portlet">
                            <div class="portlet-heading bg-primary">
                                <h3 class="portlet-title">
                                    Hasil Pencarian {{$user->name}}
                                </h3>
                                <div class="portlet-widgets">
                                    <a onClick="window.location.reload()" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#data-profile"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="data-profile" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-3">
                                            <div class="profile-detail card-box">
                                                <div>
                                                    <img src="/ubold/assets/images/users/avatar-2.jpg" class="img-circle" alt="profile-image">
                                                    <?php 
                                                        $viewes = \App\ProfileView::where('viewes_id',$user->id)->get();
                                                        $viewer = \App\ProfileView::where('viewer_id',$user->id)->get();
                                                        $follower = \App\ProfileView::where('following_id',$user->id)->get();
                                                        $following = \App\ProfileView::where('follower_id',$user->id)->get();
                                                    ?>
                                                    <ul class="list-inline status-list m-t-20">
                                                        <li>
                                                            <h3 class="text-primary m-b-5">{{count($viewes)}}</h3>
                                                            <p class="text-muted">Viewers</p>
                                                        </li>

                                                        <li>
                                                            <h3 class="text-primary m-b-5">{{count($following)}}</h3>
                                                            <p class="text-muted">Followings</p>
                                                        </li>

                                                        <li>
                                                            <h3 class="text-success m-b-5">{{count($follower)}}</h3>
                                                            <p class="text-muted">Followers</p>
                                                        </li>
                                                    </ul>

                                                    @if($user->id == \Auth::user()->id)
                                                    @else
                                                    @php $cekFollow = \App\ProfileView::where('following_id',$user->id)->where('follower_id',\Auth::user()->id)->get() @endphp
                                                    @if (count($cekFollow)>0)
                                                    <a href="/students/profile/follow/{{$user->id}}" class="btn btn-danger btn-rounded waves-effect waves-light">Following</a>
                                                    @else
                                                    <a href="/students/profile/follow/{{$user->id}}" class="btn btn-pink btn-custom btn-rounded waves-effect waves-light">Follow</a>
                                                    @endif
                                                    @endif
                                                    
                                                    <hr>
                                                    <h4 class="text-uppercase font-600">About Me</h4>
                                                    <p class="text-inverse font-13 m-b-30">
                                                        {{$user->kutipan}}
                                                    </p>

                                                    <div class="text-left text-inverse">
                                                        <p class="text-inverse font-13"><strong>Full Name :</strong> <span class="m-l-15">{{$user->name}}</span></p>

                                                        <p class="text-inverse font-13"><strong>Mobile :</strong><span class="m-l-15">{{$user->phone}}</span></p>

                                                        <p class="text-inverse font-13"><strong>Email :</strong> <span class="m-l-15">{{$user->email}}</span></p>

                                                        <p class="text-inverse font-13"><strong>Sekolah :</strong> <span class="m-l-15">{{$user->nm_sekolah}}</span></p>

                                                        <p class="text-inverse font-13"><strong>Universitas :</strong> <span class="m-l-15">{{$user->nm_universitas}}</span></p>

                                                        <p class="text-inverse font-13"><strong>Pekerjaan :</strong> <span class="m-l-15">{{$user->pekerjaan}}</span></p>

                                                    </div>


                                                    <div class="button-list m-t-20">
                                                        <a href="#modal-facebook" class="btn btn-facebook waves-effect waves-light" data-animation="door"  data-plugin="custommodal" 
                                                    	data-overlaySpeed="100" data-overlayColor="#36404a">
                                                        <i class="fa fa-facebook"></i>
                                                        </a>

                                                        <button href="#modal-twitter" class="btn btn-twitter waves-effect waves-light" data-animation="door"  data-plugin="custommodal" 
                                                    	data-overlaySpeed="100" data-overlayColor="#36404a">
                                                        <i class="fa fa-twitter"></i>
                                                        </button>

                                                        <button href="#modal-instagram" class="btn btn-instagram waves-effect waves-light" data-animation="door"  data-plugin="custommodal" 
                                                    	data-overlaySpeed="100" data-overlayColor="#36404a">
                                                        <i class="fa fa-instagram"></i>
                                                        </button>

                                                        <button href="#modal-whatsapp" class="btn btn-success waves-effect waves-light" data-animation="door"  data-plugin="custommodal" 
                                                    	data-overlaySpeed="100" data-overlayColor="#36404a">
                                                        <i class="fa fa-whatsapp"></i>
                                                        </button>

                                                    </div>
                                                </div>

                                            </div>

                                            <div class="card-box">
                                                <h4 class="m-t-0 m-b-20 header-title"><b>Keluarga <span class="text-muted">(154)</span></b></h4>

                                                <div class="friend-list">
                                                    <a href="#modal-ayah" class="waves-effect waves-light" data-animation="door"  data-plugin="custommodal" 
                                                    	data-overlaySpeed="100" data-overlayColor="#36404a">
                                                        <img src="/ubold/assets/images/users/avatar-1.jpg" class="img-circle thumb-md" alt="friend">
                                                    </a>

                                                    <a href="#modal-ibu" class="waves-effect waves-light" data-animation="door"  data-plugin="custommodal" 
                                                    	data-overlaySpeed="100" data-overlayColor="#36404a">
                                                        <img src="/ubold/assets/images/users/avatar-2.jpg" class="img-circle thumb-md" alt="friend">
                                                    </a>

                                                    <a href="#modal-wali" class="waves-effect waves-light" data-animation="door"  data-plugin="custommodal" 
                                                    	data-overlaySpeed="100" data-overlayColor="#36404a">
                                                        <img src="/ubold/assets/images/users/avatar-3.jpg" class="img-circle thumb-md" alt="friend">
                                                    </a>

                                                    <!-- <a href="#" class="text-center">
                                                        <span class="extra-number">+89</span>
                                                    </a> -->
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-lg-9 col-md-8">
                                            <form method="post" action="/students/profile/addArtikel" class="well">
                                                {{csrf_field()}}
                                                <span class="input-icon icon-right">
                                                    <textarea id="artikelnya" name="artikelnya" rows="2" class="form-control"
                                                            placeholder="Post a new message"></textarea>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-md-6"><input id="addFoto" name="url_foto" type="url" class="form-control input-sm" placeholder="http://uangkelas.com/photo1.jpg"></div>
                                                            <div class="col-md-6">
                                                                <input type="email" name="tagSiswa" id="tagSiswa" class="form-control input-sm" placeholder="owner@uangkelas.com">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                                </span>
                                                <div class="p-t-10 pull-right">
                                                    <button type="submit" class="btn btn-sm btn-primary waves-effect waves-light">Send</button>
                                                </div>
                                                <ul class="nav nav-pills profile-pills m-t-10">
                                                <li>
                                                        <a onclick="$('#addFoto').focus()"><i class=" fa fa-camera"></i></a>
                                                    </li>
                                                    <li>
                                                        <a onclick="$('#tagSiswa').focus()"><i class="fa fa-user"></i></a>
                                                    </li>
                                                    <li>
                                                        <a onclick="$('#artikelnya').focus()"><i class="fa fa-location-arrow"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><i class="fa fa-smile-o"></i></a>
                                                    </li>
                                                </ul>

                                            </form>
                                            <?php
                                                $getArtikel = \App\Artikel::where('user_id', $user->id)
                                                            ->join('users','artikels.user_id','users.id')
                                                            ->select('name','artikels.*')->orderBy('created_at','Desc')->paginate('6');
                                            
                                            ?>
                                            
                                            @foreach($getArtikel as $artikel)
                                            <?php 
                                            $responLike  = \App\ResponArtikel::join('artikels','respon_artikels.artikel_id','artikels.id')->where('artikel_id', $artikel->id)->where('respon','like')->get(); 
                                            $responDislike  = \App\ResponArtikel::join('artikels','respon_artikels.artikel_id','artikels.id')->where('artikel_id', $artikel->id)->where('respon','dislike')->get(); 
                                            $responCode  = \App\ResponArtikel::join('artikels','respon_artikels.artikel_id','artikels.id')->where('artikel_id', $artikel->id)->where('respon','code')->get(); 
                                            ?>
                                            <div class="card-box">
                                                <div class="comment">
                                                    <img src="{{$user->photo}}" alt="" class="comment-avatar">
                                                    <div class="comment-body">
                                                        <div class="comment-text">
                                                            <?php
                                                            $guest = \App\User::find($artikel->guest_id)
                                                            ?>
                                                            <div class="comment-header">
                                                                <a href="" title="">@if($artikel->guest_id != null) <a href="/students/profile/{{$guest->id}}">{{$guest->name}} </a> <span>Posted in</span> @endif {{$artikel->name}} </a><span>about 2 minuts ago</span>
                                                            </div>
                                                            {{$artikel->artikel}}
                                                            <div class="m-t-15">
                                                                <!-- <a href="">
                                                                    <img src="/ubold/assets/images/small/img1.jpg" class="thumb-md">
                                                                </a>
                                                                <a href="">
                                                                    <img src="/ubold/assets/images/small/img2.jpg" class="thumb-md">
                                                                </a>
                                                                <a href="">
                                                                    <img src="/ubold/assets/images/small/img3.jpg" class="thumb-md">
                                                                </a> -->
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="comment-footer">
                                                            <!-- AXIOS -->
                                                            <a id="like" href="/students/profile/likeArtikel/{{$artikel->id}}"><i class="fa fa-thumbs-o-up"></i>{{count($responLike)}}Like </a>
                                                            <a href="/students/profile/dislikeArtikel/{{$artikel->id}}"><i class="fa fa-thumbs-o-down"></i>{{count($responDislike)}}Dislike &nbsp</a>
                                                            <a href="/students/profile/codeArtikel/{{$artikel->id}}" class="code"><i class="fa fa-code text-warning"></i>{{number_format(count($responCode),2)* 100}}Code</a>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="m-t-30 text-center">
                                             <li>{{$getArtikel->links()}}</li>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>            
        </div>
    </div>
</div>

<!-- Modal -->
<div id="modal-facebook" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title bg-facebook">Facebook {{$user->name}}</h4>
    <div class="custom-modal-text text-center">
        <h4>{{$user->facebook}}</h4>
    </div>
</div>
<!-- Modal -->
<div id="modal-twitter" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title bg-twitter">Twitter {{$user->name}}</h4>
    <div class="custom-modal-text text-center">
        <h4>{{$user->twiter}}</h4>
    </div>
</div>
<!-- Modal -->
<div id="modal-instagram" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title bg-instagram">Instagram {{$user->name}}</h4>
    <div class="custom-modal-text text-center">
        <h4>{{$user->instagram}}</h4>
    </div>
</div>
<!-- Modal -->
<div id="modal-whatsapp" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title bg-whatsapp">WhatsApp {{$user->name}}</h4>
    <div class="custom-modal-text text-center">
        <h4>{{$user->whatsapp}}</h4>
    </div>
</div>
<!-- Modal -->
<div id="modal-ayah" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title bg-inverse">Ayah Dari {{$user->name}}</h4>
    <div class="custom-modal-text text-center">
        <h4>{{$user->ayah}}</h4>
    </div>
</div>
<!-- Modal -->
<div id="modal-ibu" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title bg-inverse">Ibu Dari {{$user->name}}</h4>
    <div class="custom-modal-text text-center">
        <h4>{{$user->ibu}}</h4>
    </div>
</div>
<!-- Modal -->
<div id="modal-wali" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title bg-inverse">Wali Dari {{$user->name}}</h4>
    <div class="custom-modal-text text-center">
        <h4>{{$user->wali}}</h4>
    </div>
</div>
<input type="hidden" id="token" name="token" value="{{csrf_field()}}">
@endsection

@section('js')
<script src="/js/axios.js"></script>
<Script>

var x = $(#token).value
function postData() {
let idLike = x;
axios.post(`/students/profile/likeArtikel/${idLike}`,{
    artikel_id: idLike
}).then((resp) => {
    console.log(`resp, id: ${idLike}`);
}).catch(error => {
    console.log(error);
})
}
postData();
console.log(x);
</Script>
@endsection