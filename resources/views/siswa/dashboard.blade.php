@extends('layouts.siswa')
@section('title')
<title>APPASMK - Wellcome</title>
@endsection
@section('css')
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="/ubold/assets/plugins/morris/morris.css">
    <link href="/ubold/assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
@endsection
@section('content')

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->                      
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="btn-group pull-right m-t-15">
                                    <button type="button" class="btn btn-default dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                                    <ul class="dropdown-menu drop-menu-right" role="menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>
                                <h4 class="page-title pt-5">APPASMK - APLIKASI PRAKERIN & ALUMNI SMK</h4>
                                <p class="text-muted page-title-alt">Welcome to APPASMK</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3 col-sm-6 col-xs-6">
                                <div class="widget-panel widget-style-2 bg-white">
                                    <i class="md md-attach-money text-primary"></i>
                                    <h2 class="m-0 text-dark counter font-600">BVC</h2>
                                    <!-- <div class="text-muted m-t-5">BUAT CV</div> -->
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-6">
                                <div class="widget-panel widget-style-2 bg-white">
                                    <i class="md md-add-shopping-cart text-pink"></i>
                                    <h2 class="m-0 text-dark counter font-600">CD</h2>
                                    <!-- <div class="text-muted m-t-5">CEK DUNIA INDUSTRI</div> -->
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-6">
                                <div class="widget-panel widget-style-2 bg-white">
                                    <i class="md md-store-mall-directory text-info"></i>
                                    <h2 class="m-0 text-dark counter font-600">CU</h2>
                                    <!-- <div class="text-muted m-t-5">CEK UNIVERSITAS</div> -->
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-6">
                                <div class="widget-panel widget-style-2 bg-white">
                                    <i class="md md-account-child text-custom"></i>
                                    <h2 class="m-0 text-dark counter font-600">AMED</h2>
                                    <!-- <div class="text-muted m-t-5">APPASMK SOSMED</div> -->
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-6">
                                <div class="widget-panel widget-style-2 bg-white">
                                    <i class="md md-account-child text-custom"></i>
                                    <h2 class="m-0 text-dark counter font-600">APAK</h2>
                                    <!-- <div class="text-muted m-t-5">APPASMK LAPAK</div> -->
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-6">
                                <div class="widget-panel widget-style-2 bg-white">
                                    <i class="md md-account-child text-custom"></i>
                                    <h2 class="m-0 text-dark counter font-600">PK</h2>
                                    <!-- <div class="text-muted m-t-5">PROFIL KU</div> -->
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-6">
                                <div class="widget-panel widget-style-2 bg-white">
                                    <i class="md md-account-child text-custom"></i>
                                    <h2 class="m-0 text-dark counter font-600">AP</h2>
                                    <!-- <div class="text-muted m-t-5">AKUN PRO</div> -->
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 col-xs-6">
                                <div class="widget-panel widget-style-2 bg-white">
                                    <i class="md md-account-child text-custom"></i>
                                    <h2 class="m-0 text-dark counter font-600">KL</h2>
                                    <!-- <div class="text-muted m-t-5">KELUAR</div> -->
                                </div>
                            </div>
                        </div>

                        

                    </div> <!-- container -->
                               
                </div> <!-- content -->

@endsection
@section('js')
<script src="/ubold/assets/plugins/moment/moment.js"></script>


<script src="/ubold/assets/plugins/morris/morris.min.js"></script>
<script src="/ubold/assets/plugins/raphael/raphael-min.js"></script>

 <script src="/ubold/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>

<!-- Todojs  -->
<script src="/ubold/assets/pages/jquery.todo.js"></script>

<!-- chatjs  -->
<script src="/ubold/assets/pages/jquery.chat.js"></script>

<script src="/ubold/assets/plugins/peity/jquery.peity.min.js"></script>

<script src="/ubold/assets/pages/jquery.dashboard_2.js"></script>
@endsection
