<div class="row">
    <div class="col-md-12">

        <div id="modal-profile" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg"> 
                <div class="modal-content"> 
                    <div class="modal-header"> 
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                        <h4 class="modal-title">Profile {{\Auth::user()->name}}</h4> 
                    </div> 
                    
                    <div class="modal-body">
                    </div> 
                </div> 
            </div>
        </div><!-- /.modal -->
        
    </div>
</div>