<!-- Modal -->
<div id="personalInfo" class="modal-demo modal-lg">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title bg-primary">Personal Info</h4>
    <div class="custom-modal-text text-inverse">
        <div class="row">
            <div class="col-md-12">
            <?php
                $profile = \App\Profileku::where('user_id',\Auth::user()->id)->first();
                $tgl_null = \Carbon\Carbon::Today()->format('l, d M Y');
                $tgl_lahir = \Carbon\Carbon::createFromFormat('Y-m-d', $profile->tgl_lahir)->format('l, d M Y');
            ?>
                <form class="form-horizontal" method="post" action="/students/resume/personal-info" role="form" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="form-group">
                            <label class="col-md-2 control-label" >Nama Lengkap</label>
                            <div class="col-md-10">
                                <input type="name" value="{{\Auth::user()->name}}" name="nama" class="form-control" placeholder="Nama Lengkap">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" >Profesi</label>
                            <div class="col-md-10">
                                <input type="name" value="{{$profile->pekerjaan}}" name="profesi" class="form-control" placeholder="Profesi">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" >Email</label>
                            <div class="col-md-10">
                                <input type="email" value="{{\Auth::user()->email}}" name="email" class="form-control" placeholder="Email Address">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label" >Alamat</label>
                            <div class="col-md-10">
                                <textarea class="form-control" name="alamat" id="" cols="30" rows="3">{{$profile->alamat}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" >Tempat Lahir</label>
                            <div class="col-md-10">
                                <input type="text" name="tmp_lahir" value="{{$profile->tmp_lahir}}" class="form-control" placeholder="Tempat Lahir">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">Tanggal Lahir</label>
                            <div class="col-md-10">
                                <div class="input-group">
                                @if($profile->tgl_lahir !== null)
                                    <input type="text" value="{{$tgl_lahir}}"  name="tgl_lahir" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
                                @else
                                    <input type="text" value="{{$tgl_null}}"  name="tgl_lahir" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
                                @endif
                                </div><!-- input-group -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" >Linkedlnr</label>
                            <div class="col-md-10">
                                <input type="name" name="linkedlnr" value="{{$profile->linkedlnr}}" class="form-control" placeholder="Linkedlnr">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" >Upload Photo</label>
                            <div class="col-md-10">
                                <input id="uplFoto" type="file" name="photo" class="filestyle form-control" data-size="sm">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <center><img src="/uploads/{{$profile->photo}}" alt="{{$profile->photo}}" class="img-responsive"></center>
                            </div>
                        </div>
                        <div class="form-group col-md-12"><input type="submit" class="btn btn-primary" value="Submit"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="experience" class="modal-demo modal-lg">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title bg-primary">Pengalaman Bekerja</h4>
    <div class="custom-modal-text text-inverse">
        <div class="row">
            <div class="col-md-12">
                <h3>Experiences</h3>
                <table class="table">
                    <tr>
                        <td>No</td>
                        <td>Start Work</td>
                        <td>Ending Work</td>
                        <td>position</td>
                        <td>At</td>
                        <td>Aksi</td>
                    </tr>
                    @php $no=1 @endphp
                    @foreach($exp as $data)
                    <tr>
                        <td>{{$no++}}</td>
                        <td>{{$data->start_bln}}, {{$data->start_thn}}</td>
                        <td>
                        @if($data->end_thn !=null ){{$data->end_bln}}, {{$data->end_thn}}
                        @else
                        Present
                        @endif
                        </td>
                        <td>{{$data->position}}</td>
                        <td>{{$data->company}}</td>
                        <td>
                            <form action="/students/resume/experience/{{$data->id}}" method="post">
                                {{csrf_field()}}
                                <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>
                <hr>
                <h3>Other Experiences</h3>
                @php 
                    $bulan = ['January', 'February','March','April','May','June','July','August','September','October','December']
                @endphp
                <form class="form-horizontal" method="post" action="/students/resume/experience" role="form" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="container">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-3 control-label" >Start Work</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="start_bln" id="start_bln">
                                        @foreach($bulan as $data)
                                        <option value="{{$data}}">{{$data}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control" name="start_thn" id="start_thn">
                                        @for($tahun=2013; $tahun <= \Carbon\Carbon::now()->year; $tahun++)
                                        <option value="{{$tahun}}">{{$tahun}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" >Ending Work</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="end_bln" id="end_bln">
                                        <option value="">Still Working</option>
                                        @foreach($bulan as $data)
                                        <option value="{{$data}}">{{$data}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control" name="end_thn" id="end_thn">
                                    <option value="">Still Working</option>
                                        @for($tahun=2013; $tahun <= \Carbon\Carbon::now()->addYears(3)->year; $tahun++)
                                        <option value="{{$tahun}}">{{$tahun}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" >Position</label>
                                <div class="col-md-8">
                                    <input type="text" name="position" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" >Company</label>
                                <div class="col-md-8">
                                <input type="text" name="company" class="form-control">
                                </div>
                            </div>
                            
                            <div class="form-group col-md-12"><input type="submit" class="btn btn-primary" value="Submit"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="education" class="modal-demo modal-lg">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title bg-primary">Pendidikan</h4>
    <div class="custom-modal-text text-inverse">
        <div class="row">
            <div class="col-md-12">
                <h3>Education</h3>
                <table class="table">
                    <tr>
                        <td>No</td>
                        <td>Start Education</td>
                        <td>Ending Education</td>
                        <td>Campus</td>
                        <td>Aksi</td>
                    </tr>
                    @php $no=1 @endphp
                    @foreach($edu as $data)
                    <tr>
                        <td>{{$no++}}</td>
                        <td>{{$data->start_bln}}, {{$data->start_thn}}</td>
                        <td>
                        @if($data->end_thn !=null ){{$data->end_bln}}, {{$data->end_thn}}
                        @else
                        Still Study
                        @endif
                        </td>
                        <td>{{$data->campus}}</td>
                        <td>
                            <form action="/students/resume/education/{{$data->id}}" method="post">
                                {{csrf_field()}}
                                <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>
                <hr>
                <h3>Other Experiences</h3>
                @php 
                    $bulan = ['January', 'February','March','April','May','June','July','August','September','October','December']
                @endphp
                <form class="form-horizontal" method="post" action="/students/resume/education" role="form" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="container">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-3 control-label" >Start Work</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="start_bln" id="start_bln">
                                        @foreach($bulan as $data)
                                        <option value="{{$data}}">{{$data}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control" name="start_thn" id="start_thn">
                                        @for($tahun=2013; $tahun < \Carbon\Carbon::now()->year; $tahun++)
                                        <option value="{{$tahun}}">{{$tahun}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" >Ending Work</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="end_bln" id="end_bln">
                                        <option value="">Still Study</option>
                                        @foreach($bulan as $data)
                                        <option value="{{$data}}">{{$data}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control" name="end_thn" id="end_thn">
                                    <option value="">Still Study</option>
                                        @for($tahun=2013; $tahun < \Carbon\Carbon::now()->addYears(3)->year; $tahun++)
                                        <option value="{{$tahun}}">{{$tahun}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" >Campus</label>
                                <div class="col-md-8">
                                    <input type="text" name="campus" class="form-control">
                                </div>
                            </div>
                            
                            <div class="form-group col-md-12"><input type="submit" class="btn btn-primary" value="Submit"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="sertifikat" class="modal-demo modal-lg">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title bg-primary">Sertifikat</h4>
    <div class="custom-modal-text text-inverse">
        <div class="row">
            <div class="col-md-12">
                <h3>Certificate</h3>
                <table class="table">
                    <tr>
                        <td>No</td>
                        <td>Date</td>
                        <td>Title</td>
                        <td>Aksi</td>
                    </tr>
                    @php $no=1 @endphp
                    @foreach($sertifikat as $data)
                    <tr>
                        <td>{{$no++}}</td>
                        <td>{{$data->start_bln}}, {{$data->start_thn}}</td>
                        <td>{{$data->sertifikat}}</td>
                        <td>
                            <form action="/students/resume/sertifikat/{{$data->id}}" method="post">
                                {{csrf_field()}}
                                <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>
                <hr>
                <h3>Other Experiences</h3>
                @php 
                    $bulan = ['January', 'February','March','April','May','June','July','August','September','October','December']
                @endphp
                <form class="form-horizontal" method="post" action="/students/resume/sertifikat" role="form" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="container">
                        <div class="row">
                        
                            <div class="form-group">
                                <label class="col-md-3 control-label" >Title</label>
                                <div class="col-md-8">
                                    <input type="text" name="sertifikat" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" >Start Work</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="start_bln" id="start_bln">
                                        @foreach($bulan as $data)
                                        <option value="{{$data}}">{{$data}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control" name="start_thn" id="start_thn">
                                        @for($tahun=2013; $tahun <= \Carbon\Carbon::now()->year; $tahun++)
                                        <option value="{{$tahun}}">{{$tahun}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group col-md-12"><input type="submit" class="btn btn-primary" value="Submit"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="skill" class="modal-demo modal-lg">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title bg-primary">Keahlian</h4>
    <div class="custom-modal-text text-inverse">
        <div class="row">
            <div class="col-md-12">
                <h3>Skill</h3>
                <table class="table">
                    <tr>
                        <td>No</td>
                        <td>Skill</td>
                        <td>Rate</td>
                        <td>Aksi</td>
                    </tr>
                    @php $no=1 @endphp
                    @foreach($sertifikat as $data)
                    <tr>
                        <td>{{$no++}}</td>
                        <td>{{$data->skill}}</td>
                        <td>
                            @php $a=$data->value @endphp
                            @for($i=1; $i<= $a; $i++)
                            <i class="fa fa-x1 fa-circle text-info"></i>
                            @endfor
                            @for($i=$a; $i< 5; $i++)
                            <i class="fa fa-x1 fa-circle text-secondary"></i>
                            @endfor
                        </td>
                        <td>
                            <form action="/students/resume/skill/{{$data->id}}" method="post">
                                {{csrf_field()}}
                                <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>
                <hr>
                <h3>Other Experiences</h3>
                @php 
                    $bulan = ['January', 'February','March','April','May','June','July','August','September','October','December']
                @endphp
                <form class="form-horizontal" method="post" action="/students/resume/skill" role="form" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="container">
                        <div class="row">
                        
                            <div class="form-group">
                                <label class="col-md-3 control-label" >Skill</label>
                                <div class="col-md-8">
                                    <input type="text" name="skill" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" >Rank</label>
                                <div class="col-md-8">
                                    <select name="value" class="selectpicker" data-style="btn-white" id="">
                                        @for($x = 1; $x<=5; $x++)
                                        <option data-icon="glyphicon-star"  value="{{$x}}">{{$x}}
                                        </option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group col-md-12"><input type="submit" class="btn btn-primary" value="Submit"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

    <!-- Modal -->
<div id="summary" class="modal-demo modal-lg">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title bg-primary">Summary</h4>
    <div class="custom-modal-text text-inverse">
        <div class="row">
            <div class="col-md-12">
                <form class="form" method="post" action="/students/resume/summary" role="form">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box">
                                <h4 class="m-b-30 m-t-0 header-title"><b>Ceritakan tentang Anda</b></h4>
                                <textarea name="konten" id="summernote">{!!$profile->summary!!}</textarea>
                                <button class="btn btn-primary" type="submit">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>