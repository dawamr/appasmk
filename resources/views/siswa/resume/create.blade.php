@extends('layouts.cv')
@section('title')
<title>APPASMK - Curriculum Vitae</title>
@endsection
@section('css')
<link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
<link href="/ubold/assets/plugins/summernote/summernote.css" rel="stylesheet" />
<style>
.content{
    background-color :#f4f4f4 !important;
    font-family: 'Noto Sans', sans-serif;
}
.fa-x1{
    font-size:13px;
    vertical-align: bottom;
}
.portlet{
    box-shadow: 0 0.5rem 2.5rem 0 rgba(0,0,0,0.25) !important;
    border-radius : 0px !important;
}
.bg-cv{
    background-color: #06b4ff;
}
.siswa-cv{
    font-weight: 700 !important;
    color: #fff !important;
    line-height: 1.2em !important;
    font-size: 4rem !important;
}
.header-cv{
    color: #ffff !important;
    margin-top: .7rem;
    font-size: 2.2rem;
    font-weight: 400;
}
.kanan-cv{
    padding-right: 2.5rem;
    padding-left: 2.5rem;
    padding-bottom: 2.5rem;
    background-color: #fff;
    color: #343434;
}
.kiri-cv{
    background-color: #f4f4f4;
    display: inline-block;
    min-height: inherit;
    vertical-align: top;
    padding-left: 2.5rem;
    padding-bottom: 2.5rem;
    padding-right: 2.5rem;
    padding-right: 2.5rem;

}
@media only screen and (min-width: 800px) {
  .kiri-cv {
    width:30%;
  }
}
@media only screen and (max-width: 600px) {
  .kiri-cv {
    width:100%;
  }
}
.p-cv{
    font-size: 1.5rem;
    color: #000000 !important;
    font-weight: normal;
}
.info-cv{
    font-weight: 700;
    font-style: bold;
    font-size: 1.5rem;
    line-height: 1.7em;
}
.summary-1{
    color: #343434; !important ;
}
.summary-2{
    font-size: 1.5rem;
    line-height: 1.7em;
    color: #343434;
}
.experience{
    padding-top: 1.5rem;
    position: relative;
}
.experience-cv{
    white-space: initial;
    vertical-align: middle;
    font-size: 2rem;
    color: #009bdf;
    font-weight: 700;
    font-style: normal;
}
.dateFrom{
    color: #343434;
    font-weight: 700;
    font-style: normal;
    line-height: 1.7em;
    font-size: 1.5rem;
}
.title-cv{
    font-size: 1.7rem;
    color: #343434;
    font-weight: 700;
    font-style: normal;
    line-height: 1.7em;
}
.subtitle-cv{
    color: #343434;
    font-weight: 400;
    font-style: italic;
    font-size: 1.5rem;
    line-height: 1.7em;
}
.profile-cv{
    box-sizing: border-box;
    width: 100%;
    max-width: 33rem;
    min-width: 100px;
    max-height: 60rem;
    height: auto;
    vertical-align: middle;
    border-style: none;
}

</style>
@endsection
@section('content')
<br>
<div class="content-page">
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">

                    <h4 class="page-title">Curriculum Vitae</h4>
                    <ol class="breadcrumb">
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li>
                            <a href="/siswa">Dashboard</a>
                        </li>
                        <li class="active">
                            Create CV
                        </li>
                    </ol>
                </div>
            </div>
            <?php
                $profile = \App\Profileku::where('user_id',\Auth::user()->id)->first();
                if($profile->tgl_lahir != null){
                  $tgl_lahir = \Carbon\Carbon::createFromFormat('Y-m-d', $profile->tgl_lahir)->format('l, d M Y');
                }else{
                  $tgl_null = \Carbon\Carbon::Today()->format('l, d M Y');
                };


            ?>
            <div class="row">
                <div class="col-lg-12">
                        <div class="portlet">
                            <div class="portlet-heading bg-cv">
                                <div class="col-md-12">
                                <h1 class="siswa-cv">
                                    {{\Auth::user()->name}}
                                </h1>
                                <h3 class=" header-cv">{{$profile->pekerjaan}}</h3>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div id="data-dudi" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                <div class="row">
                                    <div class="col-sm-8 ">
                                        <div class="col-md-12" class="summary-cv">
                                            <div class="summary-2">
                                            {!!$profile->summary!!}
                                            </div>
                                        </div>
                                        <div class="col-md-12 experience">
                                            <h3 class="experience-cv">Experience</h3>
                                            <hr>
                                            <?php 
                                                $exp = \App\Experience::where('user_id',\Auth::user()->id)->get();
                                            ?>
                                            @foreach($exp as $data)
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <span class="dateFrom">
                                                        @if($data->end_thn !=null ){{$data->start_bln}}, {{$data->start_thn}} - {{$data->end_bln}}, {{$data->end_thn}}
                                                        @else
                                                        {{$data->start_bln}}, {{$data->start_thn}} - Present
                                                        @endif
                                                    </span>
                                                </div>
                                                <div class="col-md-9">
                                                    <h1 class="title-cv"><p class="p-cv">{{$data->position}}</p></h1>
                                                    <h2 class="subtitle-cv"><p class="p-cv">{{$data->company}}</p><p class="p-cv"></p></h2>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="col-md-12 experience">
                                            <h3 class="experience-cv">Education</h3>
                                            <hr>
                                            <?php 
                                                $edu = \App\Education::where('user_id',\Auth::user()->id)->get();
                                            ?>
                                            @foreach($edu as $data)
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <span class="dateFrom">
                                                        @if($data->end_thn !=null ){{$data->start_bln}}, {{$data->start_thn}} - {{$data->end_bln}}, {{$data->end_thn}}
                                                        @else
                                                        {{$data->start_bln}}, {{$data->start_thn}} - Still Study
                                                        @endif
                                                    </span>
                                                </div>
                                                <div class="col-md-9">
                                                    <h1 class="title-cv"><p class="p-cv">{{$data->campus}}</p></h1>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="col-md-12 experience">
                                            <h3 class="experience-cv">Certificates</h3>
                                            <hr>
                                            <?php 
                                                $sertifikat = \App\Certificate::where('user_id',\Auth::user()->id)->get();
                                            ?>
                                            @foreach($sertifikat as $data)
                                            <div class="row">
                                                <div class="col-md-3"><span class="dateFrom">{{$data->start_bln}}, {{$data->start_thn}}</span></div>
                                                <div class="col-md-9">
                                                    <h1 class="title-cv"><p class="p-cv">{{$data->sertifikat}}</p></h1>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>

                                    <div class="col-sm-4 kiri-cv" >
                                        <div class="col-md-12">
                                            <div class="container"><img  class="profile-cv" src="/uploads/{{$profile->photo}}" alt="{{$profile->photo}}"></div>
                                            <h1 class="experience-cv">Personal Info</h1>
                                            <hr>
                                            <b class="summary-1">Address</b><br><br>
                                            <p class="p-cv">{{$profile->alamat}}</p>
                                            <br>
                                            <b class="summary-1">Email</b><br><br>
                                            <p class="p-cv">{{\Auth::user()->email}}</p>
                                            <br>
                                            <b class="summary-1">WhatsApp</b><br><br>
                                            <p class="p-cv">{{$profile->whatsapp}}</p>
                                            <br>
                                            <b class="summary-1">Date of Birth</b><br><br>
                                            @if($profile->tgl_lahir !== null)
                                            <p class="p-cv">{{$tgl_lahir}}</p>
                                            @else
                                            <p class="p-cv">{{$tgl_null}}</p>
                                            @endif
                                            <br>
                                            <b class="summary-1">LinkedIn</b><br><br>
                                            <p class="p-cv">{{$profile->linkedlnr}}</p>
                                            <br>
                                            <h1 class="experience-cv">SKILL</h1>
                                            <hr>
                                            <div class="row">

                                                @php $skill = \App\Skill::where('user_id',\Auth::user()->id)->get() @endphp
                                                @foreach($skill as $data)
                                                <div class="col-md-12">
                                                    <div class="col-md-6"><p class="p-cv">{{$data->skill}}</p></div>
                                                    <div class="col-md-6">
                                                        @php $a=$data->value @endphp
                                                        @for($i=1; $i<= $a; $i++)
                                                        <i class="fa fa-x1 fa-circle text-info"></i>
                                                        @endfor
                                                        @for($i=$a; $i<  5; $i++)
                                                        <i class="fa fa-x1 fa-circle text-secondary"></i>
                                                        @endfor
                                                    </div>
                                                </div>
                                                @endforeach
                                                
                                            </div>   
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="/ubold/assets/plugins/summernote/summernote.min.js"></script>
<script>
    $('#uplFoto').change(function putImage() {
        var src = document.getElementById("uplFoto");
        
        showImage(src);
    });
</script>
<script>

    jQuery(document).ready(function(){

        $('.summernote').summernote({
            height: 350,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });

        $('.inline-editor').summernote({
            airMode: true
        });

    });
</script>
@endsection
