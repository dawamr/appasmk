@extends('layouts.siswa')
@section('title')
<title>APPASMK - Curriculum Vitae</title>
@endsection
@section('css')
<link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">


<style>
.content{
    background-color :#f4f4f4 !important;
    font-family: 'Noto Sans', sans-serif;
}
.fa-x1{
    font-size:13px;
    vertical-align: bottom;
}
.portlet{
    box-shadow: 0 0.5rem 2.5rem 0 rgba(0,0,0,0.25) !important;
    border-radius : 0px !important;
}
.bg-cv{
    background-color: #06b4ff;
}
.siswa-cv{
    font-weight: 700 !important;
    color: #fff !important;
    line-height: 1.2em !important;
    font-size: 4rem !important;
}
.header-cv{
    color: #ffff !important;
    margin-top: .7rem;
    font-size: 2.2rem;
    font-weight: 400;
}
.kanan-cv{
    padding-right: 2.5rem;
    padding-left: 2.5rem;
    padding-bottom: 2.5rem;
    background-color: #fff;
    color: #343434;
}
.kiri-cv{
    background-color: #f4f4f4;
    width: 30%;
    display: inline-block;
    min-height: inherit;
    vertical-align: top;
    padding-left: 2.5rem;
    padding-bottom: 2.5rem;
    padding-right: 2.5rem;
    padding-right: 2.5rem;

}
.p-cv{
    font-size: 1.5rem;
    color: #000000 !important;
    font-weight: normal;
}
.info-cv{
    font-weight: 700;
    font-style: bold;
    font-size: 1.5rem;
    line-height: 1.7em;
}
.summary-1{
    color: #343434; !important ;
}
.summary-2{
    font-size: 1.5rem;
    line-height: 1.7em;
    color: #343434;
}
.experience{
    padding-top: 1.5rem;
    position: relative;
}
.experience-cv{
    white-space: initial;
    vertical-align: middle;
    font-size: 2rem;
    color: #009bdf;
    font-weight: 700;
    font-style: normal;
}
.dateFrom{
    color: #343434;
    font-weight: 700;
    font-style: normal;
    line-height: 1.7em;
    font-size: 1.5rem;
}
.title-cv{
    font-size: 1.7rem;
    color: #343434;
    font-weight: 700;
    font-style: normal;
    line-height: 1.7em;
}
.subtitle-cv{
    color: #343434;
    font-weight: 400;
    font-style: italic;
    font-size: 1.5rem;
    line-height: 1.7em;
}
.profile-cv{
    box-sizing: border-box;
    width: 100%;
    max-width: 33rem;
    min-width: 100px;
    max-height: 60rem;
    height: auto;
    vertical-align: middle;
    border-style: none;
}

</style>
@endsection
@section('content')
<br>
<div class="content-page">
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">

                    <h4 class="page-title">Curriculum Vitae</h4>
                    <ol class="breadcrumb">
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li>
                            <a href="/siswa">Dashboard</a>
                        </li>

                        <li class="active">
                            Create CV
                        </li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                        <div class="portlet">
                            <div class="portlet-heading bg-cv">
                                <div class="col-md-12">
                                <h1 class="siswa-cv">
                                    Dawam Raja
                                </h1>
                                <h3 class="header-cv">Full Stuck Web Developer</h3>
                                </div>
                               
                                <div class="clearfix"></div>
                            </div>
                            <div id="data-dudi" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="col-md-12" class="summary-cv">       
                                            <div class="summary-2">
                                            <b class="summary-1">Full-Stuck / Laravel Developer</b><br><br>
                                            

                                            <p class="p-cv">Over the last 2+ years, I have handle in all kinds of projects and I am able to build a web application from scratch very fast and with great efficiency or continue working on existing code with ease.</p>

                                            <p class="p-cv">Finally but not least, my ultimate goal is to satisfy clients, deliver quality work and always on time.</p>

                                            <p class="p-cv">So if you have an exciting project and looking for a skillful developer, drop me a message and we will start working on your project asap.</p>
                                            <br>
                                            <p class="p-cv"><b>SKILL :</b></p>

                                            <ul><li>Back-end (Core PHP/Laravel/NodeJS)<br></li><li>&nbsp;Front-end(VueJS)<br></li><li>&nbsp;DB (MySQL, Oracle, Mongo, Firebase)<br></li><li>&nbsp;HTML5 / XHTML / CSS3 / Javascript/Bootstrap<br></li><li>&nbsp;Wordpress / Magento / Opencart / Joomla / Drupal / Shopify<br></li><li>&nbsp;Restful API<br></li><li>&nbsp;Responsible communication for each day(24/7 online).<br></li><li>&nbsp;6 months Free bug fixing and maintenance<br><br></li></ul><p class="p-cv">Best Regards</p>

                                            <p class="p-cv"></p></div>
                                        </div>
                                        <div class="col-md-12 experience">
                                            <h3 class="experience-cv">Experience</h3>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-3"><span class="dateFrom">2018-02 - Present</span></div>
                                                <div class="col-md-9">
                                                    <h1 class="title-cv"><p class="p-cv">Software Progammer</p></h1>
                                                    <h2 class="subtitle-cv"><p class="p-cv">Santren Koding</p><p class="p-cv"></p></h2>
                                                </div>     
                                            </div>
                                        </div>
                                        <div class="col-md-12 experience">
                                            <h3 class="experience-cv">Education</h3>
                                            <hr>
                                            <div class="row">
                                            <div class="col-md-3"><span class="dateFrom">2013-07 - 2016-06</span></div>
                                            <div class="col-md-9">
                                                <h1 class="title-cv"><p class="p-cv">SMP N 23 Semarang</p></h1>
                                            </div>
                                            <div class="col-md-3"><span class="dateFrom">2016-07 - 2019-06</span></div>
                                                <div class="col-md-9">
                                                    <h1 class="title-cv"><p class="p-cv">SMK N 8 Semarang</p></h1>
                                                </div>
                                            </div>
                                            <div class="col-md-12 experience">
                                                <h3 class="experience-cv">Certificates</h3>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-3"><span class="dateFrom">2013-07</span></div>
                                                    <div class="col-md-9">
                                                        <h1 class="title-cv"><p class="p-cv">Juara 1 Dinus Application Competition 2017 Tk Nasional</p></h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-md-4 kiri-cv" >
                                        <div class="col-md-12">
                                            <div class="container"><img  class="profile-cv" src="/ubold/assets/images/users/avatar-1.jpg" alt="img-profile"></div>
                                            <h1 class="experience-cv">Personal Info</h1>
                                            <hr>
                                            <b class="summary-1">Address</b><br><br>
                                            <p class="p-cv">Jl. Duduhan,<br>
                                                Kel. Mijen, Kec. Mijen <br> Semarang, Indonesia.</p>
                                            <br>
                                            <b class="summary-1">Phone</b><br><br>
                                            <p class="p-cv">+1 636 337-2379</p>
                                            <br>
                                            <b class="summary-1">Email</b><br><br>
                                            <p class="p-cv">akunnyadawam9@gmail.com</p>
                                            <br>
                                            <b class="summary-1">Date of Birth</b><br><br>
                                            <p class="p-cv">2000-12-14</p>
                                            <br>
                                            <b class="summary-1">LinkedIn</b><br><br>
                                            <p class="p-cv">https://www.linkedin.com/in/dawamraja</p>
                                            <br>
                                            <h1 class="experience-cv">SKILL</h1>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-6"><p class="p-cv">Laravel</p></div>
                                                    <div class="col-md-6">
                                                        @php $a=4 @endphp
                                                        @for($i=1; $i<= $a; $i++)
                                                        <i class="fa fa-x1 fa-circle text-info"></i>
                                                        @endfor
                                                        @for($i=$a; $i< 5; $i++)
                                                        <i class="fa fa-x1 fa-circle text-secondary"></i>
                                                        @endfor
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="col-md-6"><p class="p-cv">VueJs</p></div>
                                                    <div class="col-md-6">
                                                        @php $a=3 @endphp
                                                        @for($i=1; $i<= $a; $i++)
                                                        <i class="fa fa-x1 fa-circle text-info"></i>
                                                        @endfor
                                                        @for($i=$a; $i< 5; $i++)
                                                        <i class="fa fa-x1 fa-circle text-secondary"></i>
                                                        @endfor
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row">
							<div class="col-sm-12">
								<div class="card-box">
									<h4 class="m-t-0 m-b-30 header-title"><b>Inline Editor</b></h4>
									<div class="inline-editor">
										
										<h3>This is an Air-mode editable area.</h3>
										<ul>
											<li>
												Select a text to reveal the toolbar.
											</li>
											<li>
												Edit rich document on-the-fly, so elastic!
											</li>
										</ul>
										<p>
											End of air-mode area
										</p>
									</div>
								</div>
							</div>
						</div> -->
                </div>
            </div>            
        </div>
    </div>
</div>
@endsection

@section('js')


@endsection