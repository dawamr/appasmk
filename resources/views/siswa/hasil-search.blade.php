@extends('layouts.siswa')
@section('title')
<title>APPASMK - Hasil Pencarian</title>
@endsection
@section('css')

@endsection
@section('content')
<br>
<div class="content-page">
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">

                    <h4 class="page-title">HASIL PENCARIAN</h4>
                    <ol class="breadcrumb">
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li>
                            <a href="/siswa">Dashboard</a>
                        </li>
                        <li class="active">
                            Hasil Pencarian
                        </li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                        <div class="portlet">
                            <div class="portlet-heading bg-primary">
                                <h3 class="portlet-title">
                                    Hasil Pencarian {{$q}}
                                </h3>
                                <div class="portlet-widgets">
                                    <a onClick="window.location.reload()" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#data-dudi"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="data-dudi" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <!-- table  -->
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-box table-responsive">
                                                @if($message = Session::get('success1'))
                                                <div class="alert alert-success">
                                                    <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                    <strong>{{$message}}</strong>
                                                </div>
                                                <hr>
                                                @endif
                                                @if($message = Session::get('wrong1'))
                                                <div class="alert alert-danger">
                                                    <button type="button" class="close" data-dismiss="alert" name="button">x</button>
                                                    <strong>{{$message}}</strong>
                                                </div>
                                                <hr>
                                                @endif

                                                <!-- <h4 class="m-t-0 header-title">
                                                    <button class="btn btn-info waves-effect waves-light" data-toggle="modal" data-target="#modal-addDudi"> Tambah Dudi</button>
                                                </h4>-->

                                                <table id="datatable-univ"
                                                        class="table dt-responsive nowrap" cellspacing="0"
                                                        width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Siswa</th>
                                                        <th>Email</th>
                                                        <th>Sekolah</th>
                                                        <th>Jurusan</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $no = 1;
                                                    
                                                    ?>
                                                    @foreach($cari as $data)
                                                    <tr>
                                                        <td>{{$no++}}</td>
                                                        <td>{{$data->name}}</td>
                                                        <td>{{$data->email}}</td>
                                                        <td>{{$data->nm_sekolah}}</td>
                                                        <td>{{$data->nm_jur}}</td>
                                                        <td>
                                                            <?php 
                                                                $pro='tidak';
                                                                
                                                                try {
                                                                    if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
                                                                        $pro = 'iya';
                                                                        $cek = \App\UserUpgrade::where('user_id',\Auth::user()->id)->first();
                                                                        if ($cek->status == 'success') {
                                                                            echo '
                                                                            <a href="/students/profile/'.$data->id.'" class="btn btn-info" > Lihat Lebih</a>';
                                                                        }
                                                                    }
                                                                } catch (\Throwable $th) {
                                                                    //throw $th;
                                                                }
                                                            ?>
                                                            @if($pro == 'tidak')
                                                            <a href="#modal-upgrade" class="btn text-warning waves-effect waves-light" data-animation="push" data-plugin="custommodal" data-overlaySpeed="500" data-overlayColor="#4c5667">Lihat Lebih</a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <!-- endtable -->
                                </div>
                            </div>
                        </div>
                </div>
            </div>            
        </div>
    </div>
</div>
@endsection

@section('js')

@endsection