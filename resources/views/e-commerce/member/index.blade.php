<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shop SNAPAN</title>
    <link href="https://fonts.googleapis.com/css?family=Karla" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap4.3.min.css">
    <style>
        body{
            font-family: 'Karla', sans-serif;
        }
        .mini-top-header{
            height: 24px;
            background-color: #201848;
            position: fixed;
            top: 0;
            width: 100%;
            z-index: 100;
            font-family: Karla,sans-serif;
            font-size: 14px;
        }
        .inner-container{margin:0 auto;box-sizing:border-box} }.inner-container{max-width:1024px}.inner-container{padding:0 20px} .mini-top-header a{color: #fff; cursor: pointer;text-decoration: none;} .mini-top-header a .help-link{margin: 4px 22px;float: right;    word-wrap: break-word;}
        .menu-navbar {
            position: fixed;
            top: 24px;
            right: 0;
            left: 0;
            background: #281e5a;
            color: #fff;
            z-index: 100;
            box-shadow: 0 0 0 0 rgba(0,0,0,.12), 0 4px 4px 0 rgba(0,0,0,.24);
            height: 65px;
            font-family: Karla,sans-serif;
        }
        .menu-navbar>h1{
            flex: 1;
            margin: 0;
            line-height: 1;
            margin-top: 13px;
        }
    </style>
</head>
<body>
    
    <header class="mini-top-header fixed-top">
        <div class="inner-container">
            <div class="container">
                <a href="http://uangkelas.com">
                    <p class="help-link mr-5">Bantuan</p>
                </a>
            </div>
        </div>
    </header>
    <header class="menu-navbar">
        <nav class="navbar navbar-expand-lg navbar-dark bg-purple container">
            <a class="navbar-brand ml-5" href="#">Shop SNAPAN</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                
            </div>
        </nav>

    </header>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                    <img src="https://cdn.gramedia.com/uploads/images/ransel_miiko_oranye__w640_hauto.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                    <img src="https://cdn.gramedia.com/uploads/images/ransel_miiko_oranye__w640_hauto.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                    <img src="https://cdn.gramedia.com/uploads/images/ransel_miiko_oranye__w640_hauto.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                    <img src="https://cdn.gramedia.com/uploads/images/ransel_miiko_oranye__w640_hauto.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


<script src="/js/jquery.min.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap4.3.min.js"></script>
</body>
</html>