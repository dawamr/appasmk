<!DOCTYPE html>
<html>
	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
		<meta name="author" content="Coderthemes">

		<link rel="shortcut icon" href="/ubold/assets/images/favicon_1.ico">

		@yield('title')
        <link href="/ubold/assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
        <link href="/ubold/assets/plugins/summernote/summernote.css" rel="stylesheet" />
        <link href="/ubold/assets/plugins/custombox/css/custombox.css" rel="stylesheet">

		<link href="/ubold/assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
		<link href="/ubold/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link href="/ubold/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="/ubold/assets/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
        <link href="/ubold/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <link href="/ubold/assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/ubold/assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/ubold/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/ubold/assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/ubold/assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
        <link href="/ubold/assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/ubold/assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>

        <link href="/ubold/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <style>
            a{
                color: #00c4f1;
            }
            .download{
                color:white;
            }
            .download:hover{
                color: white !important;
            }
        </style>
        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        @yield('css')
        <style>
        table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before{
            content: '~';
            background-color: #00c4f1 !important;
        }
        table{
            font-size : default;
        }
        </style>
        <script src="/ubold/assets/js/modernizr.min.js"></script>

	</head>

	<body class="fixed-left">
    <?php $user = \App\Profileku::where('user_id',\Auth::user()->id)->first();
        ?>
		<!-- Begin page -->
		<div id="wrapper">


            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <a href="index.html" class="logo"><i class="icon-c-logo">SMK</i><span>APPASMK</span></a>
                        <!-- Image Logo here -->
                        <!--<a href="index.html" class="logo">-->
                            <!--<i class="icon-c-logo"> <img src="/ubold/assets/images/logo_sm.png" height="42"/> </i>-->
                            <!--<span><img src="/ubold/assets/images/logo_light.png" height="20"/></span>-->
                        <!--</a>-->
                    </div>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" style="background-color:#00c4f1" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left waves-effect waves-light">
                                    <i class="md md-menu"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>

                            <ul class="nav navbar-nav hidden-xs">
                                <li><a href="#" class="waves-effect waves-light">Files</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown"
                                       role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span
                                            class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </li>
                            </ul>

                            <form role="search" class="navbar-left app-search pull-left hidden-xs">
			                     <input type="text" placeholder="Search..." class="form-control">
			                     <a href=""><i class="fa fa-search"></i></a>
			                </form>


                            <ul class="nav navbar-nav navbar-right pull-right">
                                <li class="hidden-xs">
                                    <br>
                                    <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                                </li>
                                <li class="dropdown top-menu-item-xs">
                                    <br>
                                    <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"><img src="/ubold/assets/images/users/avatar-1.jpg" alt="user-img" class="img-circle"> </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0)"><i class="ti-user m-r-10 text-custom"></i> Profile</a></li>
                                        <li><a href="javascript:void(0)"><i class="ti-settings m-r-10 text-custom"></i> Settings</a></li>
                                        <li><a href="javascript:void(0)"><i class="ti-lock m-r-10 text-custom"></i> Lock screen</a></li>
                                        <li class="divider"></li>
                                        <li><a href="/logout"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->

             <!-- Right Sidebar -->
             <div class="left side-menu">
                    <div class="sidebar-inner slimscrollleft">
                        <!--- Divider -->
                        <div id="sidebar-menu">
                            <ul>

                                <li class="text-muted menu-title">Navigation</li>

                                <li class="has_sub">
                                    <a href="/home" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span></a>
                                </li>
                                <?php
                                    $pro='tidak';

                                    try {
                                        if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
                                            $pro = 'iya';
                                            $cek = \App\UserUpgrade::where('user_id',\Auth::user()->id)->first();
                                            // if ($cek->status == 'success') {
                                            //     echo '
                                            //     <a href="/students/profile/'.$data->id.'" class="btn btn-info" > Lihat Lebih</a>';
                                            // }
                                        }
                                    } catch (\Throwable $th) {
                                        //throw $th;
                                    }
                                ?>


                                <li class="has_sub">
                                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-light-bulb"></i><span> Profiles </span> <span class="menu-arrow"></span> </a>
                                    <ul class="list-unstyled">
                                        <li><a  href="#modal-profile" class="waves-effect waves-light" data-toggle="modal"></i> <span> Profile Siswa </span></a></li>
                                    </ul>
                                </li>

                                <li class="has_sub">
                                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-light-bulb"></i><span> FILL IN YOUR DOCUMENT </span> <span class="menu-arrow"></span> </a>
                                    <ul class="list-unstyled">
                                        <li> <a href="#personalInfo" class="waves-effect waves-light" data-animation="push" data-plugin="custommodal" data-overlaySpeed="500" data-overlayColor="#323a42">
                                        Personal Info</a></li>
                                        <li> <a href="#summary" class="waves-effect waves-light" data-animation="push" data-plugin="custommodal" data-overlaySpeed="500" data-overlayColor="#323a42">
                                        Summary</a></li>
                                        <li> <a href="#experience" class="waves-effect waves-light" data-animation="push" data-plugin="custommodal" data-overlaySpeed="500" data-overlayColor="#323a42">
                                        Experience</a></li>
                                        <li> <a href="#education" class="waves-effect waves-light" data-animation="push" data-plugin="custommodal" data-overlaySpeed="500" data-overlayColor="#323a42">
                                        Education</a></li>
                                        <li> <a href="#sertifikat" class="waves-effect waves-light" data-animation="push" data-plugin="custommodal" data-overlaySpeed="500" data-overlayColor="#323a42">
                                        Certifikates</a></li>
                                        <li> <a href="#skill" class="waves-effect waves-light" data-animation="push" data-plugin="custommodal" data-overlaySpeed="500" data-overlayColor="#323a42">
                                        Skill</a></li>
                                    </ul>
                                </li>

                                <li class="has_sub">
                                    <a href="/logout"><i class="ti-gift"></i><span> Keluar </span></a>
                                </li>

                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            <!-- /Right-bar -->
            @yield('content')

            @include('siswa.resume.modal-cv')

            <!-- Modal -->
			<div id="modal-upgrade" class="modal-demo">
			    <button type="button" class="close" onclick="Custombox.close();">
			        <span>&times;</span><span class="sr-only">Close</span>
			    </button>
			    <h4 class="custom-modal-title bg-danger">Upgrade Account!</h4>
			    <div class="custom-modal-text text-inverse">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <form class="form" method="get" action="/upgrade-account" role="form"> -->

                                <div class="form-group">
                                    <label class="control-label text-danger" >Upgrade Account For :</label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                            <i class="ion-checkmark-circled text-success"></i> Search All Students Registered <br>
                                            <i class="ion-checkmark-circled text-success"></i> View Students Information
                                            </div>
                                            <div class="col-md-6">
                                            <i class="ion-checkmark-circled text-success"></i> View More Students Information <br>
                                            <i class="ion-checkmark-circled text-success"></i> Change Email Address <br>
                                            <i class="ion-checkmark-circled text-success"></i> View Users Click Your Profiles <br>
                                            <i class="ion-checkmark-circled text-success"></i> Use Sosial Media APPASMK <br>
                                            <i class="ion-checkmark-circled text-success"></i> Create Curriculum Vitae <br>

                                            </div>
                                        </div>
                                        <div class="col-md-12">

                                            <a href="/upgrade-account" class="btn btn-danger">IDR {{number_format(25000,2)}}</a>

                                        </div>
                                    </div>
                                </div> <!-- form-group -->
                            <!-- </form> -->
                        </div>
                    </div>
			    </div>
			</div>



            <footer class="footer">
            © 2019. UangKelas.Com
            </footer>

    </div>
        <!-- END wrapper -->

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="/ubold/assets/js/jquery.min.js"></script>
        <script src="/ubold/assets/js/bootstrap.min.js"></script>
        <script src="/ubold/assets/js/detect.js"></script>
        <script src="/ubold/assets/js/fastclick.js"></script>
        <script src="/ubold/assets/js/jquery.slimscroll.js"></script>
        <script src="/ubold/assets/js/jquery.blockUI.js"></script>
        <script src="/ubold/assets/js/waves.js"></script>
        <script src="/ubold/assets/js/wow.min.js"></script>
        <script src="/ubold/assets/js/jquery.nicescroll.js"></script>
        <script src="/ubold/assets/js/jquery.scrollTo.min.js"></script>
        <script src="/ubold/assets/plugins/moment/moment.js"></script>
     	<script src="/ubold/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
     	<script src="/ubold/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
     	<script src="/ubold/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
     	<script src="/ubold/assets/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
        <script src="/ubold/assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="/ubold/assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="/ubold/assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>

     	<script src="/ubold/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="/ubold/assets/pages/jquery.form-pickers.init.js"></script>
        <script src="/ubold/assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
        @yield('js')
        <script src="/ubold/assets/js/jquery.core.js"></script>
        <script src="/ubold/assets/js/jquery.app.js"></script>

        <script src="/ubold/assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="/ubold/assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="/ubold/assets/plugins/datatables/jszip.min.js"></script>
        <script src="/ubold/assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="/ubold/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <!-- Modal-Effect -->
        <script src="/ubold/assets/plugins/custombox/js/custombox.min.js"></script>
        <script src="/ubold/assets/plugins/custombox/js/legacy.min.js"></script>

        <script type="text/javascript">
        $(document).ready(function () {
            $('#datatable').dataTable();
            $('#datatable-dudi').DataTable({
                "lengthMenu": [15, 30, 50, 100, "All"]
            });
            $('#datatable-univ').DataTable({
                "lengthMenu": [15, 30, 50, 100, "All"]
            });

        });
        </script>
        <!--form validation init-->
        <script src="/ubold/assets/plugins/summernote/summernote.min.js"></script>
        <script>
            var tipe = 'html';
            $(document).ready(function() {
                $('#summernote').summernote({
                    height: 350,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor
                    focus: false                 // set focus to editable area after initializing summernote
                });
                render();
            });
        </script>
	</body>
</html>
