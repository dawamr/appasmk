<!DOCTYPE html>
<html>
	<head>

    <?php
    
   ?>
   
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
		<meta name="author" content="Coderthemes">

		<link rel="shortcut icon" href="/ubold/assets/images/favicon_1.ico">

		@yield('title')
        <link href="/ubold/assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />


		<link href="/ubold/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="/ubold/assets/css/responsive.css" rel="stylesheet" type="text/css" />
		<link href="/ubold/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link href="/ubold/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="/ubold/assets/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
        <link href="/ubold/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <link href="/ubold/assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/ubold/assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/ubold/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/ubold/assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/ubold/assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
        <link href="/ubold/assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/ubold/assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <style>
            a{
                color: #00c4f1;
            }
            .download{
                color:white;
            }
            .download:hover{
                color: white !important;
            }
        </style>
        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        @yield('css')
        <style>
        table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before{
            content: '~';
            background-color: #00c4f1 !important;
        }
        table{
            font-size : default;
        }
        </style>
        <script src="/ubold/assets/js/modernizr.min.js"></script>

	</head>

	<body class="fixed-left">

		<!-- Begin page -->
		<div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <a href="index.html" class="logo"><i class="icon-c-logo">SMK</i><span>APPASMK</span></a>
                        <!-- Image Logo here -->
                        <!--<a href="index.html" class="logo">-->
                            <!--<i class="icon-c-logo"> <img src="/ubold/assets/images/logo_sm.png" height="42"/> </i>-->
                            <!--<span><img src="/ubold/assets/images/logo_light.png" height="20"/></span>-->
                        <!--</a>-->
                    </div>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" style="background-color:#00c4f1" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left waves-effect waves-light">
                                    <i class="md md-menu"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>

                            <ul class="nav navbar-nav hidden-xs">
                                <li><a href="#" class="waves-effect waves-light">Files</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown"
                                       role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span
                                            class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </li>
                            </ul>

                            <form role="search" class="navbar-left app-search pull-left hidden-xs">
			                     <input type="text" placeholder="Search..." class="form-control">
			                     <a href=""><i class="fa fa-search"></i></a>
			                </form>


                            <ul class="nav navbar-nav navbar-right pull-right">
                                <li class="hidden-xs">
                                    <br>
                                    <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                                </li>
                                <li class="dropdown top-menu-item-xs">
                                    <br>
                                    <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"><img src="/ubold/assets/images/users/avatar-1.jpg" alt="user-img" class="img-circle"> </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0)"><i class="ti-user m-r-10 text-custom"></i> Profile</a></li>
                                        <li><a href="javascript:void(0)"><i class="ti-settings m-r-10 text-custom"></i> Settings</a></li>
                                        <li><a href="javascript:void(0)"><i class="ti-lock m-r-10 text-custom"></i> Lock screen</a></li>
                                        <li class="divider"></li>
                                        <li><a href="/logout"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->

             <!-- Right Sidebar -->
             <div class="left side-menu">
                    <div class="sidebar-inner slimscrollleft">
                        <!--- Divider -->
                        <div id="sidebar-menu">
                            <ul>
            
                                <li class="text-muted menu-title">Navigation</li>
            
                                <li class="has_sub">
                                    <a href="/home" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span></a>
                                </li>
            
                                <!-- <li class="has_sub">
                                    <a href="/bendahara/kas" class="waves-effect"><i class="ti-paint-bucket"></i> <span> KAS KELAS </span> </a>
                                </li> -->

                                <li class="has_sub">
                                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-light-bulb"></i><span> Management Sekolah </span> <span class="menu-arrow"></span> </a>
                                    <ul class="list-unstyled">
                                        <li><a style="cursor: pointer;" data-toggle="modal" data-target="#modal-jurusan">Manage Jurusan</a></li>
                                        <li><a style="cursor: pointer;" data-toggle="modal" data-target="#modal-kelas">Manage Kelas</a></li>
                                        <li><a style="cursor: pointer;" data-toggle="modal" data-target="#modal-siswa">Manage Siswa</a></li>
                                        <li><a style="cursor: pointer;" data-toggle="modal" data-target="#modal-token">Manage Token</a></li>
                                    </ul>
                                </li>
            
                                <li class="has_sub">
                                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-light-bulb"></i><span> Management PKL </span> <span class="menu-arrow"></span> </a>
                                    <ul class="list-unstyled">
                                        <li><a href="/admin/dudi">Manage Dudi</a></li>
                                        <li><a href="/admin/magang">Manage Magang</a></li>
                                    </ul>
                                </li>

                                <li class="has_sub">
                                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-light-bulb"></i><span> Management Alumni </span> <span class="menu-arrow"></span> </a>
                                    <ul class="list-unstyled">
                                        <li><a style="cursor: pointer;" data-toggle="modal" data-target="#modal-univ">Manage Universitas</a></li>
                                        <li><a style="cursor: pointer;" data-toggle="modal" data-target="#modal-alumni">Manage Alumni</a></li>
                                    </ul>
                                </li>
            
                                <li class="has_sub">
                                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-spray"></i> <span> Laporan </span> <span class="menu-arrow"></span> </a>
                                    <ul class="list-unstyled">
                                        <li><a style="cursor: pointer;" data-toggle="modal" data-target="#modal-laporanPKL">Laporan PKL</a></li>
                                        <li><a style="cursor: pointer;" data-toggle="modal" data-target="#modal-bulan">Laporan Alumni</a></li>
                                    </ul>
                                </li>
            
            
                                <li class="text-muted menu-title">More</li>
                                <li class="has_sub">
                                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-gift"></i><span> Pengaturan </span> <span class="menu-arrow"></span></a>
                                    <ul class="list-unstyled">
                                        <li><a style="cursor: pointer;" data-toggle="modal" data-target="#modal-setprofile">Akun</a></li>
                                    </ul>
                                </li>
            
                                <li class="has_sub">
                                    <a href="javascript:void(0);"><i class="ti-gift"></i><span> Keluar </span></a>
                                </li>
            
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            <!-- /Right-bar -->

            @yield('content')
            @include('admin.modal-sekolah')
            @include('admin.modal-alumni')
            @include('admin.modal-laporan')   

            <footer class="footer">
            © 2019. UangKelas.Com
            </footer>

    </div>
        <!-- END wrapper -->

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="/ubold/assets/js/jquery.min.js"></script>
        <script src="/ubold/assets/js/bootstrap.min.js"></script>
        <script src="/ubold/assets/js/detect.js"></script>
        <script src="/ubold/assets/js/fastclick.js"></script>
        <script src="/ubold/assets/js/jquery.slimscroll.js"></script>
        <script src="/ubold/assets/js/jquery.blockUI.js"></script>
        <script src="/ubold/assets/js/waves.js"></script>
        <script src="/ubold/assets/js/wow.min.js"></script>
        <script src="/ubold/assets/js/jquery.nicescroll.js"></script>
        <script src="/ubold/assets/js/jquery.scrollTo.min.js"></script>
        <script src="/ubold/assets/plugins/moment/moment.js"></script>
     	<script src="/ubold/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
     	<script src="/ubold/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
     	<script src="/ubold/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
     	<script src="/ubold/assets/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
        <script src="/ubold/assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="/ubold/assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>

     	<script src="/ubold/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="/ubold/assets/pages/jquery.form-pickers.init.js"></script>
        <script src="/ubold/assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
        @yield('js')
        <script src="/ubold/assets/js/jquery.core.js"></script>
        <script src="/ubold/assets/js/jquery.app.js"></script>
        
        <script src="/ubold/assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="/ubold/assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="/ubold/assets/plugins/datatables/jszip.min.js"></script>
        <script src="/ubold/assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="/ubold/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script type="text/javascript">
        $(document).ready(function () {
            $('#datatable').dataTable();
            $('#datatable-dudi').DataTable();
            $('#datatable-magang').DataTable({
                "lengthMenu": [10, 20, 40, 100, "All"]
            });
            $('#datatable-addSiswa').DataTable({
		         "lengthMenu":  [5, 10, 25, "All"]
		      });
            $('#datatable-tambahDudi').DataTable();
            $('#datatable-kelas').DataTable({
                "lengthMenu": [5, 10, 25, "All"]
            });
            $('#datatable-siswa').DataTable({
                "lengthMenu": [10, 15 ,25, "All"]
            });
            $('#datatable-addAlumni').DataTable({
                "lengthMenu": [15, 30 ,50, "All"]
            });
            $('#datatable-alumni').DataTable({
                "lengthMenu": [15, 30 ,50, "All"]
            });
            $('#datatable-universitas').DataTable({
                "lengthMenu": [10, 25, 50, "All"]
            });
            $('#datatable-laporanPkl').DataTable({
                "lengthMenu": [10, 20, 40, "All"]
            })
            $('#datatable-jurusan').DataTable();
        });
</script>
	</body>
</html>