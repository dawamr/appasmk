@extends('layouts.app')
@section('css')
<style>
    .custom-card{
        box-shadow: 1px 1px 1px !important;
    }
    p{
        font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
    }
</style>

@endsection
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header"><h3 class="h-block">Upgrade Account {{\Auth::user()->name}}</h3></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="post" action="/upgrade-account" id="form">
                        {{csrf_field()}}
                    <center>
                    
                            <h4>Paymet With {{$metode}}</h4>

                        @if($metode=="M-Banking")
                        <h3>Rp {{number_format(20000,2)}}</h3>
                        @else
                        <h3>Rp {{number_format(25000,2)}}</h3>
                        @endif
                        <br>
                        <h4>Transfer Number</h4>
                        @if($metode=='PULSA')
                        <h6>088228832803</h6>
                        @endif
                        @if($metode=='PULSA')
                        <h6>088228832803</h6>
                        @endif
                        @if($metode=='M-Banking')
                        <div class="col-md-5">
                            <h4 id="norek"></h4>
                        </div>
                        <div class="col-md-5">
                        <select id="mbank" name="provider" onchange="displaySelect();" class="form-control input-sm">
                            <option value="">-- Select Providers --</option>
                            <option value="BCA"> => m-BCA & KLIK BCA</option>
                            <option value="MANDIRI"> => MANDIRI ONLINE & MANDIRI INTERNET</option>
                            <option value="BNI"> => MOBILE BANKING BNI & IBANK PERSONAL BNI</option>
                            <option value="CIMB NIAGA"> => CIMB NIAGA</option>
                            <option value="BRI"> => BRI</option>
                        </select>
                        <script>
                            function displaySelect(){
                            var d= document.getElementById("mbank");
                            var display = d.options[d.selectedIndex].value;
                            if(display <1){
                                document.getElementById("norek").innerHTML ="" ;
                            }
                            if(display == "BCA"){
                                document.getElementById("norek").innerHTML ="39358088228832803 <br>";
                            }
                            if(display == "MANDIRI"){
                                document.getElementById("norek").innerHTML = "088228832803 <br>";
                            }
                            if(display == "BNI"){
                                document.getElementById("norek").innerHTML = "8740088228832803 <br>";
                            }
                            if(display == "CIMB NIAGA"){
                                document.getElementById("norek").innerHTML = "9 0882-2883-2803 <br>";
                            }
                            if(display == "BRI"){
                                document.getElementById("norek").innerHTML = "9 0882-2883-2803 <br>";
                            }

                            
                            }
                        </script>
                        </div>
                        @endif
                        @if($metode=='OVO')
                        <h6>088228832803</h6>
                        @endif
                        @if($metode=='minimart')
                        <div class="col-md-5">
                            <h4 id="norek"></h4>
                        </div>
                        <div class="col-md-5">
                            <select id="mbank" name="provider" onchange="displaySelect();" class="form-control input-sm">
                                <option value="">-- Select Providers --</option>
                                <option value="1"> => Alfamart</option>
                            </select>
                            <script>
                                function displaySelect(){
                                    var d= document.getElementById("mbank");
                                    var display = d.options[d.selectedIndex].value;
                                    if(display <1){
                                        document.getElementById("norek").innerHTML ="" ;
                                    }
                                    if(display == 1){
                                        document.getElementById("norek").innerHTML =" 088228832803 <br>" ;
                                    }
                                }
                            </script>
                        </div>
                        
                        @endif
                        <hr>
                        <br>
                        <h5>Click Confirm to confirm your payment</h5>
                        <div class="row">
                            <div class="col-lg-3 col-2 col-xs-0"></div>
                            <div class="col-lg-6 col-md-6 col-12"> 
                                <div class="card">
                                    <div class="card-body">
                                        @php $udah = 'belum'  @endphp
                                        
                                        
                                        @if($udah=='belum')
                                        
                                            <div class="form-group">
                                                <label for="tanggal_lahir">Date of Transfer</label>
                                                <!-- <input type="date" class="form-control" name="tanggal_lahir" id="tanggal_lahir" aria-describedby="emailHelp" placeholder="" required> -->
                                                <br>
                                                <input type="date"  value="{{\Carbon\Carbon::today()->format('d/m/Y')}}" class="form-control" id="tanggal_transfer" name="tanggal_transfer" >
                                            </div>
                                            <input type="hidden" name="metode" value="{{$metode}}">
                                            <div class="form-group">
                                                <label for="alamat">Sender's Name</label>
                                                <input value="{{\Auth::user()->name}}" type="text" class="form-control" name="nama" id="nama" aria-describedby="emailHelp" placeholder="" disabled>
                                            </div>


                                            <button type="submit" class="h-block btn btn-success waves-effect waves-light">Confirm Payment</button>

                                        @endif
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-lg-4 col-2 col-xs-0"></div>
                        </div>
                    </center>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header"><h3 class="h-block">How To Transfer</h3></div>

                <div class="card-body">
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
