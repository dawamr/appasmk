@extends('layouts.app')
@section('css')
<style>
    .custom-card{
        box-shadow: 1px 1px 1px !important;
    }
    p{
        font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
    }
</style>
@endsection
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header"><h3 class="h-block">Upgrade Account {{\Auth::user()->name}}</h3></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card custom-card">
                                <div class="col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="col">
                                            <br>
                                            <h4>#1 {{\Auth::user()->name}} [{{\Carbon\Carbon::today()->format('Y-m-d')}}]</h4>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-10 col-sm-12">
                                            <p>Upgrae Account</p>
                                            <p>[ APPASMK - Account ]
                                        </div>
                                        <div class="col-md-2 col-sm-12">
                                            <p><strong>Rp 25,000.00</strong></p>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <!-- <div class="col" align="left">
                                            <a href="/lombaku/2/peserta/create" class="h-block btn btn-outline-success waves-effect waves-light float-left tambah-peserta">Add Participant</a>
                                        </div> -->
                                        <div class="col" align="right">
                                            <a href="/home" class="h-block btn btn-primary waves-effect waves-light">Back</a>
                                            <a href="/upgrade-account/next" class="h-block btn btn-success waves-effect waves-light">Next</a>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
