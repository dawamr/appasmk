@extends('layouts.app')
@section('css')
<style>
    .custom-card{
        box-shadow: 1px 1px 1px !important;
    }
    p{
        font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
    }
</style>

@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header"><h3 class="h-block">How To Transfer</h3></div>

                <div class="card-body">
                  
                    <?php 
                    try {
                        if($cek->status == 'success'){
                            echo '<h3>Thanks, Payment Success !</h3>
                                    <a href="/home" class="btn btn-primary">Back Dashboard</a>';
                        }
                        if($cek->status == 'request'){
                            echo '<h3>Thanks your payment</h3>
                            <center><a href="https://wa.me/088228832804?text='. \Auth::user()->name .'_PleaseConfrims_DateTransfer_'. $cek->date_tf .'" class="btn btn-success">Chat in WhatsApp</a>
                            <br><span>or</span><br> Chats in WhatsApp 088228832804
                            </center>';
                        }
                        
                    } catch (\Throwable $th) {
                        //throw $th;
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection