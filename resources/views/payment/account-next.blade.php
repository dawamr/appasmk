@extends('layouts.app')
@section('css')
<style>
    .custom-card{
        box-shadow: 1px 1px 1px !important;
    }
    p{
        font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
    }
</style>
@endsection
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header"><h3 class="h-block">Konfrimasi !</h3></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card custom-card">
                                <div class="col-md-12 col-lg-12">
                                    <div class="table-responsive">
                                        <table style="font-family: monospace;font-size: 14px;"class="table table-sm">
                                            <thead>
                                                <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Name</th>
                                                <th scope="col">Category</th>
                                                <th scope="col">Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                                <tr>
                                                <th scope="row">1</th>
                                                <td>{{\Auth::user()->name}}</td>
                                                <td>APPASMK - Account </td>
                                                <td>Rp 25,000.00</td>
                                                </tr>
                                                    <th scope="row"></th>
                                                    <td align="right" colspan="3"><h5>Total: Rp 25,000.00</h5></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        

                                    </div>
                                    <form action="/upgrade-account/konfirmasi" method="post" class="form">
                                        <h5>Select Payment Method</h5>
                                            {{csrf_field()}}
                                        <div class="col-md-3">
                                            <select name="metode" id="" class="form-control">
                                                <option value="pulsa">PULSA</option>
                                                <option value="ovo">OVO</option>
                                                <option value="dana">DANA</option>
                                                <option value="minimart">MINIMARKET</option>
                                                <option value="atm">ATM</option>
                                                <option value="mbank">M-BANKING (disc 20%)</option>
                                            </select>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-3" align="center">
                                                <div class="form-group">
                                                    <button class="btn btn-danger waves-effect waves-light">Kembali</button>
                                                    <button class="btn btn-success waves-effect waves-light">konfirmasi</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <br>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
