<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function dashboard(){
        $data['sekolah_id'] = \App\Admin::where('user_id', \Auth::user()->id)->pluck('sekolah_id')->first();
        $sekolah = \App\Sekolah::find($data['sekolah_id']);
        $data['sekolah'] = $sekolah;
        $data['jurusan'] = \App\Jurusan::where('sekolah_id',$data['sekolah_id'])->get();

        $siswa =\App\Jurusan::where('sekolah_id',$data['sekolah_id'])
                ->join('kelas', 'kelas.jurusan_id', '=', 'jurusans.id')
                ->join('angkatans', 'angkatans.id', '=', 'kelas.angkatan_id')
                ->join('profilekus', 'kelas.id', '=', 'profilekus.kelas_id')
                ->join('users', 'users.id', '=', 'profilekus.user_id')
                ->join('role_user','role_user.user_id','users.id')
                ->where('role_id',3)
                ->select('users.name as nm_siswa','users.created_at as created_at','profilekus.status as status','users.id as id','jurusans.nama as nm_jur', 'kelas.nama as nm_kelas', 'angkatans.angkatan as angkatan');
        $data['siswa'] = $siswa->orderBy('created_at','Desc')
                                ->get();
        $data['siswaaktif'] = $data['siswa']->where('status','siswa');
        $data['alumni'] = $data['siswa']->where('status','alumni');
        $data['kelas'] = \App\Jurusan::where('sekolah_id',$data['sekolah_id'])->join('kelas', 'kelas.jurusan_id', '=', 'jurusans.id')
                        ->join('angkatans', 'angkatans.id', '=', 'kelas.angkatan_id')
                        ->select('kelas.id as id','jurusans.nama as nm_jur', 'kelas.nama as nm_kelas', 'angkatans.angkatan as angkatan')
                        ->orderBy('nm_kelas','Asc')
                        ->get();
        $data['dudi'] = \App\Dudi::where('sekolah_id', $data['sekolah_id'])->get();
        $data['newuser'] = $siswa->paginate(8);
        return view('admin.dashboard')->with($data);
    }
    public function alumni(){
        $data['sekolah_id'] = \App\Admin::where('user_id', \Auth::user()->id)->pluck('sekolah_id')->first();
        $sekolah = \App\Sekolah::find($data['sekolah_id']);
        $data['sekolah'] = $sekolah;
        $data['jurusan'] = \App\Jurusan::where('sekolah_id',$data['sekolah_id'])->get();

        $siswa =\App\Jurusan::where('sekolah_id',$data['sekolah_id'])
                ->join('kelas', 'kelas.jurusan_id', '=', 'jurusans.id')
                ->join('angkatans', 'angkatans.id', '=', 'kelas.angkatan_id')
                ->join('profilekus', 'kelas.id', '=', 'profilekus.kelas_id')
                ->join('users', 'users.id', '=', 'profilekus.user_id')
                ->join('role_user','role_user.user_id','users.id')
                ->where('role_id',3)
                ->select('users.name as nm_siswa','users.created_at as created_at','profilekus.status as status','users.id as id','jurusans.nama as nm_jur', 'kelas.nama as nm_kelas', 'angkatans.angkatan as angkatan');
        $data['siswa'] = $siswa->orderBy('created_at','Desc')
                                ->get();
        $data['siswaaktif'] = $data['siswa']->where('status','siswa');
        $data['alumni'] = $data['siswa']->where('status','alumni');
        $data['kelas'] = \App\Jurusan::where('sekolah_id',$data['sekolah_id'])->join('kelas', 'kelas.jurusan_id', '=', 'jurusans.id')
                        ->join('angkatans', 'angkatans.id', '=', 'kelas.angkatan_id')
                        ->select('kelas.id as id','jurusans.nama as nm_jur', 'kelas.nama as nm_kelas', 'angkatans.angkatan as angkatan')
                        ->orderBy('nm_kelas','Asc')
                        ->get();
        $data['dudi'] = \App\Dudi::where('sekolah_id', $data['sekolah_id'])->get();
        $data['newuser'] = $siswa->paginate(8);
        return view('admin.alumni.index')->with($data);
    }
    public function magang(){
        $data['sekolah_id'] = \App\Admin::where('user_id', \Auth::user()->id)->pluck('sekolah_id')->first();
        $sekolah = \App\Sekolah::find($data['sekolah_id']);
        $data['sekolah'] = $sekolah;
        $data['jurusan'] = \App\Jurusan::where('sekolah_id',$data['sekolah_id'])->get();

        $siswa =\App\Jurusan::where('sekolah_id',$data['sekolah_id'])
                ->join('kelas', 'kelas.jurusan_id', '=', 'jurusans.id')
                ->join('angkatans', 'angkatans.id', '=', 'kelas.angkatan_id')
                ->join('profilekus', 'kelas.id', '=', 'profilekus.kelas_id')
                ->join('users', 'users.id', '=', 'profilekus.user_id')
                ->join('role_user','role_user.user_id','users.id')
                ->where('role_id',3)
                ->select('users.name as nm_siswa','users.created_at as created_at','profilekus.status as status','users.id as id','jurusans.nama as nm_jur', 'kelas.nama as nm_kelas', 'angkatans.angkatan as angkatan');
        $data['siswa'] = $siswa->orderBy('created_at','Desc')
                                ->get();
        $data['siswaaktif'] = $data['siswa']->where('status','siswa');
        $data['alumni'] = $data['siswa']->where('status','alumni');
        $data['kelas'] = \App\Jurusan::where('sekolah_id',$data['sekolah_id'])->join('kelas', 'kelas.jurusan_id', '=', 'jurusans.id')
                        ->join('angkatans', 'angkatans.id', '=', 'kelas.angkatan_id')
                        ->select('kelas.id as id','jurusans.nama as nm_jur', 'kelas.nama as nm_kelas', 'angkatans.angkatan as angkatan')
                        ->orderBy('nm_kelas','Asc')
                        ->get();
        $data['dudi'] = \App\Dudi::where('sekolah_id', $data['sekolah_id'])->get();
        $data['newuser'] = $siswa->paginate(8);
        return view('admin.PKL.magang')->with($data);
    }
    public function dudi(){
        $data['sekolah_id'] = \App\Admin::where('user_id', \Auth::user()->id)->pluck('sekolah_id')->first();
        $sekolah = \App\Sekolah::find($data['sekolah_id']);
        $data['sekolah'] = $sekolah;
        $data['jurusan'] = \App\Jurusan::where('sekolah_id',$data['sekolah_id'])->get();

        $siswa =\App\Jurusan::where('sekolah_id',$data['sekolah_id'])
                ->join('kelas', 'kelas.jurusan_id', '=', 'jurusans.id')
                ->join('angkatans', 'angkatans.id', '=', 'kelas.angkatan_id')
                ->join('profilekus', 'kelas.id', '=', 'profilekus.kelas_id')
                ->join('users', 'users.id', '=', 'profilekus.user_id')
                ->join('role_user','role_user.user_id','users.id')
                ->where('role_id',3)
                ->select('users.name as nm_siswa','users.created_at as created_at','profilekus.status as status','users.id as id','jurusans.nama as nm_jur', 'kelas.nama as nm_kelas', 'angkatans.angkatan as angkatan');
        $data['siswa'] = $siswa->orderBy('created_at','Desc')
                                ->get();
        $data['siswaaktif'] = $data['siswa']->where('status','siswa');
        $data['alumni'] = $data['siswa']->where('status','alumni');
        $data['kelas'] = \App\Jurusan::where('sekolah_id',$data['sekolah_id'])->join('kelas', 'kelas.jurusan_id', '=', 'jurusans.id')
                        ->join('angkatans', 'angkatans.id', '=', 'kelas.angkatan_id')
                        ->select('kelas.id as id','jurusans.nama as nm_jur', 'kelas.nama as nm_kelas', 'angkatans.angkatan as angkatan')
                        ->orderBy('nm_kelas','Asc')
                        ->get();
        $data['dudi'] = \App\Dudi::where('sekolah_id', $data['sekolah_id'])->get();
        $data['newuser'] = $siswa->paginate(8);
        return view('admin.PKL.index')->with($data);
    }
    public function siswa(){
        $data['sekolah_id'] = \App\Admin::where('user_id', \Auth::user()->id)->pluck('sekolah_id')->first();
        $sekolah = \App\Sekolah::find($data['sekolah_id']);
        $data['sekolah'] = $sekolah;
        $data['jurusan'] = \App\Jurusan::where('sekolah_id',$data['sekolah_id'])->get();

        $siswa =\App\Jurusan::where('sekolah_id',$data['sekolah_id'])
                ->join('kelas', 'kelas.jurusan_id', '=', 'jurusans.id')
                ->join('angkatans', 'angkatans.id', '=', 'kelas.angkatan_id')
                ->join('profilekus', 'kelas.id', '=', 'profilekus.kelas_id')
                ->join('users', 'users.id', '=', 'profilekus.user_id')
                ->join('role_user','role_user.user_id','users.id')
                ->where('role_id',3)
                ->select('users.name as nm_siswa','users.created_at as created_at','profilekus.status as status','users.id as id','jurusans.nama as nm_jur', 'kelas.nama as nm_kelas', 'angkatans.angkatan as angkatan');
        $data['siswa'] = $siswa->orderBy('created_at','Desc')
                                ->get();
        $data['siswaaktif'] = $data['siswa']->where('status','siswa');
        $data['alumni'] = $data['siswa']->where('status','alumni');
        $data['kelas'] = \App\Jurusan::where('sekolah_id',$data['sekolah_id'])->join('kelas', 'kelas.jurusan_id', '=', 'jurusans.id')
                        ->join('angkatans', 'angkatans.id', '=', 'kelas.angkatan_id')
                        ->select('kelas.id as id','jurusans.nama as nm_jur', 'kelas.nama as nm_kelas', 'angkatans.angkatan as angkatan')
                        ->orderBy('nm_kelas','Asc')
                        ->get();
        $data['dudi'] = \App\Dudi::where('sekolah_id', $data['sekolah_id'])->get();
        $data['newuser'] = $siswa->paginate(8);
        return view('admin.sekolah.siswa')->with($data);
    }

    public function queryUser(Request $request)
    {
        $id = \App\Admin::where('user_id', \Auth::user()->id)->pluck('sekolah_id')->first();
        $data = \App\User::join('role_user','users.id','role_user.user_id')
        ->where('name','LIKE','%'.$request->nm_siswa.'%')
        ->join('profilekus','users.id','profilekus.user_id')
        ->join('kelas','profilekus.kelas_id','kelas.id')
        ->join('jurusans','kelas.jurusan_id','jurusans.id')
        ->join('sekolahs','jurusans.sekolah_id','sekolahs.id')
        ->where('sekolahs.id', $id)
        ->select('users.name as name')
        ->get();
        return response()->json($data);      
    }
    public function queryDudi(Request $request){
        $id = \App\Admin::where('user_id', \Auth::user()->id)->pluck('sekolah_id')->first();
        $data = \App\Dudi::where('sekolah_id', $id)->where('nm_dudi','LIKE','%'.$request->dudi)->select('nm_dudi as name')->get();
        return response()->json($data); 
    }

    public function addJurusan(Request $request){
        for($i=0; $i<count($request->jurusan); $i++){
            if($request->jurusan[$i] !== null){
                $jurusan[$i] = new \App\Jurusan;
                $jurusan[$i]->nama = $request->jurusan[$i];
                $jurusan[$i]->sekolah_id = $request->sekolah_id; 
                $jurusan[$i]->save();
            }
           
        }
        return redirect()->back();
    }
    public function addKelas(Request $request){
        // dd($request);
        for($i=0; $i<count($request->nm_kelas); $i++){
            if($request->nm_kelas[$i] !== null){
                $kelas[$i] = new \App\Kelas;
                $kelas[$i]->nama = $request->nm_kelas[$i];
                $kelas[$i]->jurusan_id = $request->jurusan_id[$i][0]; 
                $kelas[$i]->angkatan_id = $request->angkatan_id[$i][0]; 
                $kelas[$i]->save();
            }
           
        }
        return redirect()->back();
    }
    public function delJurusan($id){
        $jur= \App\Jurusan::find($id);
        $jur->delete();

        return redirect()->back();
    }
    public function delKelas($id){
        $jur= \App\Kelas::find($id);
        $jur->delete();

        return redirect()->back();
    }
    public function addSiswa(Request $request){
        $cek = [];
        for($i=0; $i<count($request->nm_siswa); $i++){
            if($request->nm_siswa[$i] !== null){
                $siswa[$i] = new \App\User;
                $siswa[$i]->name = $request->nm_siswa[$i];            
                $siswa[$i]->email = strtolower(str_replace(" ","",$request->nm_siswa[$i]))."@uangkelas.com";
                $siswa[$i]->password = bcrypt("rahasia");
                $siswa[$i]->save();
                \DB::insert('insert into role_user (role_id,user_id,user_type) values (?, ?,?)', [3, $siswa[$i]->id,'App\User']);
                $cek[$i] = \App\Kelas::where('nama',$request->nm_kelas[$i])->where('angkatan_id',$request->angkatan_id[$i])->pluck('id');
                // dd($cek[$i][0]);
                $profileku[$i] = new \App\Profileku;
                $profileku[$i]->user_id = $siswa[$i]->id;
                $profileku[$i]->kelas_id = $cek[$i][0]; 
                $profileku[$i]->status = $request->status;
                $profileku[$i]->save();
            }
        }
        // dd($ce/ssk);
        
        return redirect()->back();
    }
    public function addDudi(Request $request){
        // dd($request);
        try {
            for($i=0; $i<count($request->nm_dudi); $i++){
                $cekKelas[$i] = \App\Kelas::where('nama',$request->nm_kelas[$i])->where('angkatan_id', $request->angkatan_id[$i])->pluck('id');
                if($request->nm_dudi[$i] !== null){
                    $dudi[$i] = new \App\Dudi;
                    $dudi[$i]->nm_dudi = $request->nm_dudi[$i];
                    $dudi[$i]->email = $request->email[$i]; 
                    $dudi[$i]->alamat = $request->alamat[$i];
                    $dudi[$i]->telp = $request->telp[$i];
                    $dudi[$i]->web = $request->website[$i]; 
                    $dudi[$i]->kpl_dudi = $request->hrd[$i];
                    $dudi[$i]->sekolah_id = $request->sekolah_id;
                    $dudi[$i]->save();
                }
            }
        } catch (\Throwable $th) {
            // throw $th;
        }
        return redirect()->back();
    }
    public function delDudi($id){
        $dudi= \App\Dudi::find($id);
        $dudi->delete();

        return redirect()->back();
    }
    public function addMagang(Request $request){
        // dd($request);
        try {
            $cekMagang = \App\Magang::pluck('siswa_id');
           
            for($i=0; $i<count($request->nm_siswa); $i++){
                $cekSiswa[$i] = \App\User::join('role_user','users.id','role_user.user_id')->where('role_id', 3)->where('name', $request->nm_siswa[$i])->pluck('id');
                $cekDudi[$i] = \App\Dudi::where('nm_dudi', $request->dudi[$i])->pluck('id');
                if($request->nm_siswa[$i] !==null && $cekDudi !==null && $cekSiswa !==null ){
                    $magang[$i] = new \App\Magang;
                    $magang[$i]->siswa_id =$cekSiswa[$i][0];
                    $magang[$i]->dudi_id =$cekDudi[$i][0]; 
                    $magang[$i]->angkatan_id = $request->angkatan_id[$i];
                    $magang[$i]->magang = $request->lama[$i][0]; 
                    $magang[$i]->save();
                }
                
            }
            // dd($cekMagang);
        } catch (\Throwable $th) {
            // throw $th;
        }
        return redirect()->back();
    }
    public function addAlumni(Request $request){
        // dd($request);
        for ($i=0; $i <count($request->cek) ; $i++) { 
            if($request->cek[$i] === 'on'){
                $alumni[$i] = \App\Profileku::find($request->alumni_id[$i]);
                $alumni[$i]->status = "alumni";
                $alumni[$i]->save();
            }
        }
        return redirect()->back();
    }

    public function genToken(Request $request){
        
        $char = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $string = '';
        for ($i=0; $i < 6; $i++) { 
            $pos = rand(0, strlen($char)-1);
            $string .= $char{$pos};
        }
        $data = \App\Sekolah::find($request->sekolah_id);
        $data->token = $string;
        $data->exp_token = \Carbon\Carbon::now()->addDays(3)->format('Y-m-d');
        $data->save();
        return redirect()->back();
    }

    public function addUniversitas(Request $request){
        $cekResult = \App\Universitas::pluck('nm_universitas');
             
        for ($i=0; $i < count($request->nm_univ); $i++) { 
            if ( $request->nm_univ[$i] !==null && $request->alamat[$i] !==null) {
                try {
                    $univ[$i] = new \App\Universitas;
                    $univ[$i]->nm_universitas = $request->nm_univ[$i];
                    $univ[$i]->alamat = $request->alamat[$i];
                    $univ[$i]->email = $request->email[$i];
                    $univ[$i]->telp= $request->telp[$i];
                    $univ[$i]->web = $request->web[$i];
                    $univ[$i]->save();
                } catch (\Throwable $th) {
                   return $th;
                } finally{
                    return redirect()->back();
                }
            }
        }
    }

    public function lap1Html($id){
        $data['id'] =$id;
        return view('admin.html.lap1')->with($data);
    }
   

}
