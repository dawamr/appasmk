<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UpgradeController extends Controller
{
    public function account(Request $request){
        // $app = $request->app;
        return view('payment.account-up');
    }
    public function next(Request $request){
        // $app = $request->app;
        return view('payment.account-next');
    }
    public function konfirmasi(Request $request){
        // $app = $request->app;
        $metode = "";
        if($request->metode == "pulsa"){
            $data['metode'] = "PULSA";
        }
        if($request->metode == "ovo"){
            $data['metode'] = "OVO";
        }
        if($request->metode == "dana"){
            $data['metode'] = "DANA";
        }
        if($request->metode == "atm"){
            $data['metode'] = "ATM";
        }
        if($request->metode == "mbank"){
            $data['metode'] = "M-Banking";
        }
        if($request->metode == "minimart"){
            $data['metode'] = "minimart";
        }
        return view('payment.account-pembayaran')->with($data);
    }
    public function thanks(Request $request){
        // $app = $request->app;
        
        $upgrade = new \App\UserUpgrade;
        $upgrade->user_id = \Auth::user()->id;
        $upgrade->status= 'request';
        $upgrade->date_tf = $request->tanggal_transfer;
        $upgrade->metode = $request->metode . " - " . $request->provider;
        $upgrade->save();

        return redirect('/upgrade-account/success');
    }
    public function success(Request $request){
        // $app = $request->app;
            if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
                $cek = \App\UserUpgrade::where('user_id',\Auth::user()->id)->first();
                return view('payment.account-success')->with('cek',$cek);
            }else{
                return redirect()->back();
            }
    }
}
