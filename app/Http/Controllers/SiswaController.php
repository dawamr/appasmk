<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Uuid;

class SiswaController extends Controller
{
    public function dashboard(){
        return view('siswa.dashboard');
    }
    public function register(Request $request){
        $email = strtolower(str_replace(" ","",$request->name))."@uangkelas.com";
        $cekemail = \App\User::where('email', $email)->get();
        $cekToken = \App\Sekolah::where('token',$request->token)->first();
        $sekolah_id =0;
        // dd(\Carbon\Carbon::create($cekToken->exp_token)->subDay);
        if($cekToken !== null ){
            if($cekToken->exp_token > \Carbon\Carbon::now()->format('Y-m-d')){
                $sekolah_id = $cekToken->id;
            }else{
                return redirect()->back();
            }
        }else{
            return redirect()->back();
        }
        if(count($cekemail) >0){
            return redirect('/login');
            // $email = strtolower(str_replace(" ","",$request->name)).date('ymd')."@uangkelas.com";
            // $cekemail = \App\User::where('email', $email)->get();
            // if(count($cekemail) >0){
            //     $email = strtolower(str_replace(" ","",$request->name)).date('ymdh')."@uangkelas.com";
            // }
        }
        $user = new \App\User();
        $user->name = $request->name;
        $user->email = $email;
        $user->password = bcrypt($request->password);
        $user->save();
        \DB::table('role_user')->insert([
            'role_id' => 3,
            'user_id' =>$user->id,
            'user_type' => 'App\User',
        ]);
        // $profile = new \App\Profileku();
        // $profile->user_id = $user->id;
        // $profil->status = 'siswa';
        // $profile->save();
        $data['email'] = $email;
        $data['sekolah_id'] = $sekolah_id;
        return view('auth.setting')->with($data);
    }

    public function update_user(Request $request){
        $cekUser = \App\User::where('email',$request->email2)->first();
        $profile = new \App\Profileku();
        $profile->user_id = $cekUser->id;
        $profile->status = $request->status;
        $profile->kelas_id = $request->kelas_id;
        $profile->save();
        return redirect('login');
    }

    public function dudi(){
        $data['sekolah_id'] = \Auth::user()->join('profilekus','users.id','profilekus.user_id')
                                ->join('kelas','profilekus.kelas_id','kelas.id')
                                ->join('jurusans','kelas.jurusan_id','jurusans.id')
                                ->select('sekolah_id')->pluck('sekolah_id');
        $data['sekolah'] = \App\Sekolah::find($data['sekolah_id']);
        $data['jurusan'] = \App\Jurusan::where('sekolah_id',$data['sekolah_id'])->pluck('nama');
        return view('siswa.dudi')->with($data);
    }
    public function univ(){
        return view('siswa.universitas');
    }

    public function update(Request $request){
        // dd($request->tgl_lahir);

        if($request->tgl_lahir !== null){
            $tgl = \Carbon\Carbon::createFromFormat('D, d M Y', $request->tgl_lahir)->format('Y-m-d');
        }
        $user = \App\User::find(\Auth::user()->id);
        if($request->nama !== null){
            $user->name = $request->nama;
        }
        if($request->password !== null){
            $user->password = $request->pasword;
        }
        $user->save();

        $update = \App\Profileku::where('user_id',\Auth::user()->id)->first();
        $update->alamat = $request->alamat;
        $update->tmp_lahir = $request->tmp_lahir;
        $update->tgl_lahir = $tgl;
        $update->no_ktp = $request->ktp;
        $update->nm_ibu = $request->ibu;
        $update->nm_ayah = $request->ayah;
        $update->nm_wali = $request->wali;
        $update->telp = $request->telp;
        $update->whatsapp = $request->whatsapp;
        $update->facebook = $request->facebook;
        $update->twiter = $request->twitter;
        $update->instagram = $request->instagram;
        $update->pekerjaan = $request->pekerjaan;
        $update->almt_pekerjaan = $request->almt_kerja;
        $update->pghsl_bulanan = $request->pghsl;
        $update->photo = $request->foto;
        $update->kutipan = $request->kutipan;
        $update->save();

        return redirect()->back();
    }
    public function search(Request $request){
        $history = new \App\SearchHistory;
        $history->user_id = \Auth::user()->id;
        $history->query = $request->qname;
        $history->save();
        $data['q'] = $request->qname;
        $data['cari'] = \App\User::join('role_user','users.id','role_user.user_id')
                ->where('name','LIKE','%'.$request->qname.'%')
                ->join('profilekus','users.id','profilekus.user_id')
                ->join('kelas','profilekus.kelas_id','kelas.id')
                ->join('jurusans','kelas.jurusan_id','jurusans.id')
                ->join('sekolahs','jurusans.sekolah_id','sekolahs.id')
                ->select('users.id as id','name','users.email as email','sekolahs.nama as nm_sekolah','jurusans.nama as nm_jur')
                ->get();
        // dd($data);
        return view('siswa.hasil-search')->with($data);
    }

    public function profileMore($id){
        if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
            $user = \App\User::where('users.id',$id)->join('profilekus','users.id','profilekus.user_id')
                    ->join('kelas','profilekus.kelas_id','kelas.id')
                    ->join('jurusans','kelas.jurusan_id','jurusans.id')
                   ->join('sekolahs','jurusans.sekolah_id','sekolahs.id')

                  ->join('angkatans','kelas.angkatan_id','angkatans.id')
                    ->select('users.name as name','users.email as email',
                             'profilekus.*','users.id as id','kelas.nama as nm_kelas','jurusans.nama as nm_jur',
                             'sekolahs.nama as nm_sekolah','angkatan')
                    ->first();
                   try {
                         $cekView = \App\ProfileView::where('viewes_id',$user->id)->where('viewer_id',\Auth::user()->id)->get();
                        if(count($cekView)<1){
                        $view = new \App\ProfileView;
                        $view->viewes_id = $user->id;
                        $view->viewer_id= \Auth::user()->id;
                        $view->save();
                        }
                   } catch (\Throwable $th) {
                    //    return redirect()->back();
                   }

            return view('siswa.profile-more')->with('user',$user);
        }else{
            return redirect()->back();
        }
    }
    public function addArtikel(Request $request){
        // dd($request);
        if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
            $artikel = new \App\Artikel;
            $artikel->user_id = $request->user_id;
            $artikel->guest_id = \Auth::user()->id;
            $artikel->artikel = $request->artikelnya;
            $artikel->save();
            return redirect()->back();
        }else {
            return redirect('/upgrade-account');
        }

    }
    public function likeArtikel($id){
        // dd($request);
        if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
            $cekUser = \App\ResponArtikel::where('user_id',\Auth::user()->id)
                        ->where('artikel_id',$id)->where('respon','like')->get();
            if(count($cekUser)>0){
                return redirect()->back();
            }else{
                $like = new \App\ResponArtikel;
                $like->artikel_id = $id;
                $like->respon = 'like';
                $like->user_id = \Auth::user()->id;
                $like->save();
                return redirect()->back();
            }
        }else {
            return redirect('/upgrade-account');
        }

    }
    public function dislikeArtikel($id){
        // dd($request);
        if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
             $cekUser = \App\ResponArtikel::where('user_id',\Auth::user()->id)
                        ->where('artikel_id',$id)->where('respon','dislike')->get();
            if(count($cekUser)>0){
                return redirect()->back();
            }else{
                $dislike = new \App\ResponArtikel;
                $dislike->artikel_id = $id;
                $dislike->respon = 'dislike';
                $dislike->user_id = \Auth::user()->id;
                $dislike->save();
                return redirect()->back();
            }
        }else {
            return redirect('/upgrade-account');
        }
    }

    public function codeArtikel($id){
        // dd($request);
        if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
             $cekUser = \App\ResponArtikel::where('user_id',\Auth::user()->id)
                        ->where('artikel_id',$id)->where('respon','code')->get();
            if(count($cekUser)>0){
                return redirect()->back();
            }else{
                if (count(\App\Artikel::where('id',$id)->where('user_id',\Auth::user()->id)->get()) >0) {
                    return redirect()->back();
                }else{
                    $code = new \App\ResponArtikel;
                    $code->artikel_id = $id;
                    $code->respon = 'code';
                    $code->user_id = \Auth::user()->id;
                    $code->save();
                    return redirect()->back();
                }
            }
        }else {
            return redirect('/upgrade-account');
        }

    }
    public function follow($id){
        // dd($request);
        if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
            $cekFollow = \App\ProfileView::where('following_id',$id)->where('follower_id',\Auth::user()->id)->get();
            if (count($cekFollow) >0) {
                $unfollow = \App\ProfileView::where('following_id',$id)->where('follower_id',\Auth::user()->id)->first();
                $delRow = \App\profileView::find($unfollow->id);
                $delRow->delete();
                return redirect()->back();
            }else{
            $follow = new \App\ProfileView;
            $follow->following_id = $id;
            $follow->follower_id= \Auth::user()->id;
            $follow->save();
            return redirect()->back();
            }
        }else {
            return redirect('/upgrade-account');
        }
    }
    public function resume(){
        // if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
            return view('siswa.resume.index');
        // }else {
        //     return redirect('/upgrade-account');
        // }
    }
    public function createCV(){
        if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
            return view('siswa.resume.create');
        }else {
            return redirect('/upgrade-account');
        }
    }
    public function summary(Request $request){
        if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
            $id =  \App\Profileku::where('user_id',\Auth::user()->id)->pluck('id')->first();
            $summary = \App\Profileku::find($id);
            $summary->summary = $request->konten;
            $summary->save();
            return redirect()->back();
            // dd($request);
        }else {
            return redirect('/upgrade-account');
        }
    }
    public function personalInfo(Request $request){
        // dd($request);
        if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
           $profil = \App\Profileku::where('user_id',\Auth::user()->id)->first();
           $profil->pekerjaan = $request->profesi;
           $profil->alamat = $request->alamat;
           $profil->tmp_lahir = $request->tmp_lahir;
           $profil->tgl_lahir = \Carbon\Carbon::createFromFormat('l, d M Y', $request->tgl_lahir)->format('Y-m-d');
           $profil->linkedlnr = $request->linkedlnr;
           $exPhoto = $profil->photo;
           if (file_exists('uploads/'.$exPhoto)){
                Storage::delete('uploads/'.$exPhoto);
           }
           
           
           $file_name = "";
            $uuid = Uuid::generate(1);
          
        //   dd($uuid);
           if(Input::File('photo')){
               $file   = Input::file('photo');
            //    $extension = Input::file('photo')->getClientOriginalExtension();
            //     $photo = Input::file('photo')->getClientOriginalName();
            //     $photo = md5($photo);
               $file_name = $uuid.".".$file->getClientOriginalExtension();
            // $file_name = $photo.".".$file->getClientOriginalExtension();
               $path = public_path('uploads/' .$file_name);
                //    $file->move('uploads',  $photo.".".$extension);
                $img = \Image::make($file->getRealPath());
                $img->resize(null, 700, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save($path);
                $profil->photo =  $file_name;
           }

           $profil->save();

           $usr = \App\User::find(\Auth::user()->id);
           $usr->name= $request->nama;
           $usr->email= $request->email;
           $usr->save();
           return redirect()->back();
        }else {
            return redirect('/upgrade-account');
        }
    }
    public function experience(Request $request){
        if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
            $experience = new \App\Experience;
            $experience->user_id = \Auth::user()->id;
            $experience->start_bln = $request->start_bln;
            $experience->start_thn =$request->start_thn;
            $experience->end_thn =$request->end_thn;
            $experience->end_bln =$request->end_bln;
            $experience->position = $request->position;
            $experience->company =$request->company;
            $experience->save();
            return redirect()->back();
        }else {
            return redirect('/upgrade-account');
        }
    }
    public function delExperience($id){
        if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
            $del = \App\Experience::find($id);
            $del->delete();

            return redirect()->back();
        }else {
            return redirect('/upgrade-account');
        }
        
    }
    public function education(Request $request){
        if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
            $experience = new \App\Education;
            $experience->user_id = \Auth::user()->id;
            $experience->start_bln = $request->start_bln;
            $experience->start_thn =$request->start_thn;
            $experience->end_thn =$request->end_thn;
            $experience->end_bln =$request->end_bln;
            $experience->campus = $request->campus;
            $experience->save();
            return redirect()->back();
        }else {
            return redirect('/upgrade-account');
        }
    }
    public function delEducation($id){
        if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
            $del = \App\Education::find($id);
            $del->delete();

            return redirect()->back();
        }else {
            return redirect('/upgrade-account');
        }
        
    }
    public function sertifikat(Request $request){
        if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
            $sertifikat = new \App\Certificate;
            $sertifikat->user_id = \Auth::user()->id;
            $sertifikat->start_bln = $request->start_bln;
            $sertifikat->start_thn =$request->start_thn;
            $sertifikat->sertifikat = $request->sertifikat;
            $sertifikat->save();
            return redirect()->back();
        }else {
            return redirect('/upgrade-account');
        }
    }
    public function delSertifikat($id){
        if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
            $del = \App\Certificate::find($id);
            $del->delete();

            return redirect()->back();
        }else {
            return redirect('/upgrade-account');
        }
        
    }
    public function skill(Request $request){
        if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
            $skill = new \App\Skill;
            $skill->user_id = \Auth::user()->id;
            $skill->skill = $request->skill;
            $skill->value = $request->value;
            $skill->save();
            return redirect()->back();
        }else {
            return redirect('/upgrade-account');
        }
    }
    public function delSkill($id){
        if(count(\App\UserUpgrade::where('user_id',\Auth::user()->id)->get()) >0){
            $del = \App\Skill::find($id);
            $del->delete();

            return redirect()->back();
        }else {
            return redirect('/upgrade-account');
        }
        
    }
    
}
