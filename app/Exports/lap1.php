<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;

class xls_lap1 implements \Maatwebsite\Excel\Concerns\FromView
{
  var $data;

  public function __construct($data) {
    $this->data = $data;
  }
  public function view(): View {
    $data = $this->data;
    return view('admin.html.lap1', $data);
  }
}


?>
